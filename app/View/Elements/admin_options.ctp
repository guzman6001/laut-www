<?php 

$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}

if($isok) { ?>
		
<center><div id="admin_options-text">
<?php echo $this->Html->link(__('New Link'), array('controller' => 'links', 'action' => 'add')); ?> |

<?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> |

<?php echo $this->Html->link(__('New Video'), array('controller' => 'videos', 'action' => 'add'));?> |

<?php echo $this->Html->link(__('New Image'), array('controller' => 'images', 'action' => 'add'));?> |

<?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add'));?> |

<?php echo $this->Html->link(__('New Comic'), array('controller' => 'comics', 'action' => 'add'));?> |

<?php echo $this->Html->link(__('New Quote'), array('controller' => 'quotes', 'action' => 'add'));?> |

<?php echo $this->Html->link(__('New Song'), array('controller' => 'songs', 'action' => 'add'));?> |

<?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout')); ?>
</div></center>
<?php } ?>
