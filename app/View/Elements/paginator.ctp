﻿<script type="text/javascript" src="/ias/jquery.ias.js">
jQuery.ias({
  container 	: ".container",
  		// Enter the selector of the element containing
  		// your items that you want to paginate.
  
  item		: ".post",
		// Enter the selector of the element that each
		// item has. Make sure the elements are inside
		// the container element.
 
  pagination	: ".paging",
		// Enter the selector of the element that contains
		// your regular pagination links, like next,
		// previous and the page numbers. This element
		// will be hidden when IAS loads.
 
  next		: ".next a",
		// Enter the selector of the link element that
		// links to the next page. The href attribute
		// of this element will be used to get the items
		// from the next page.
 
  loader	: "images/loader.gif"
		// Enter the url to the loader image. This image
		// will be displayed when the next page with items
		// is loaded via AJAX.
});
</script>

<style type="text/css">
/** Paging **/
.paging {
	/* background:#fff; */
	color: #ccc;
	margin-top: 1em;
	clear:both;
}
.paging .current,
.paging .disabled,
.paging a {
	text-decoration: none;
	padding: 5px 5px;
	display: inline-block
}
.paging > span {
	display: inline-block;
	border: 1px solid #ccc;
	border-left: 0;
}
.paging > span:hover {
	background: #efefef;
}
.paging .prev {
	border-left: 1px solid #ccc;
	-moz-border-radius: 4px 0 0 4px;
	-webkit-border-radius: 4px 0 0 4px;
	border-radius: 4px 0 0 4px;
}
.paging .next {
	-moz-border-radius: 0 4px 4px 0;
	-webkit-border-radius: 0 4px 4px 0;
	border-radius: 0 4px 4px 0;
}
.paging .disabled {
	color: #ddd;
}
.paging .disabled:hover {
	background: transparent;
}
.paging .current {
	background: #efefef;
	color: #c73e14;
}
</style>
<div><center>
	<div class="paging">
	<?php
	echo $this->Paginator->first('<< ' . __('first'), array(), null, array('class' => 'first disabled'));
	echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
	echo $this->Paginator->numbers(array('separator' => ''));
	echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	echo $this->Paginator->last(__('last') . ' >>', array(), null, array('class' => 'last disabled'));
	?>
	</div>
<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}')
	));
	?>	
</p>
</div></center>
