<script type='text/javascript'>
		var ie =false;
		if ('\v'=='v')
		{
			ie=true;
		}
		else
		{
			var ie = (window.external && typeof window.XMLHttpRequest == "undefined");
		}
		if (ie)
		{
			document.write('<div style="background-color:red;color:yellow;font-size:90%;"><p align="justify">Se ha detectado que usas el navegador Internet Explorer en cualquiera de sus versiones, lo cual limita tu experiencia en cualquier sitio web, ya que:<br /><br /> - No cumple con los estándares web actuales (se mantiene obsoleto a lo largo de sus versiones).<br /> - Es lento.<br /> - Es inseguro.<br /><br />Te recomendamos el uso de cualquiera de estas alternativas:<br /> - <a href="http://www.mozilla-europe.org/es/firefox/" target="_blank" style="color:white;">Mozilla Firefox</a> (Veloz, Seguro, Estable y 100% Libre).<br /> - <a href="http://www.google.com/chrome" target="_blank" style="color:white;">Google Chrome</a> (Bastante seguro y rápido).<br /> - <a href="http://www.opera.com/download/" target="_blank" style="color:white;">Opera</a> (Vanguardista).<br /> - <a href="http://www.apple.com/es/safari/" target="_blank" style="color:white;">Safari</a> (Bastante rápido).</p></div>');
		}
		</script>