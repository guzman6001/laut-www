﻿<center>
<br />

<?php echo $this->Html->link(
	$this->Html->image('icons/home.png', 
	array('alt'=> __('Home', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'sitio','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/comics.png', 
	array('alt'=> __('Comics', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'comics','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/blog.png', 
	array('alt'=> __('Blog', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'posts','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/fotos.png', 
	array('alt'=> __('Fotos', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'fotos','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/nanoblog.png', 
	array('alt'=> __('Nanoblog', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'microposts','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/images.png', 
	array('alt'=> __('Images', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'images','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/videos.png', 
	array('alt'=> __('Videos', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'videos','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/musica.png', 
	array('alt'=> __('Música', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'songs','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/frases.png', 
	array('alt'=> __('Frases', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'quotes','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/links.png', 
	array('alt'=> __('Links', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'links','action'=>'index'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/eventos.png', 
	array('alt'=> __('Eventos', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'sitio','action'=>'eventos'), 
	array('escape' => false));
?>
<br /><br /> 
<?php echo $this->Html->link( 
	$this->Html->image('icons/search.png', 
	array('alt'=> __('Buscar', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'sitio','action'=>'buscar'), 
	array('escape' => false));
?>
<br /><br />
<?php echo $this->Html->link(
	$this->Html->image('icons/login.png', 
	array('alt'=> __('Login', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'users','action'=>'login'), 
	array('escape' => false));
?><br /><br />
</center>
