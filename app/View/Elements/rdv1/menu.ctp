﻿<?php 

$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}

if($isok) { ?>
<div id="navi">
	<ul>
		<li><a href="/" title="Inicio">Inicio</a></li>
		<li><a href="#" title="Registro">Herramientas</a></li>
		<li><a href="/faqs" title="FAQS">FAQS</a></li>
		<li><a href="/usuario/salir" title="Salir">Salir</a></li>
	</ul>
</div>
<?php } else {?>
<div id="navi">
	<ul>
	
		<li><a href="/" title="Inicio">Inicio</a></li>
		<li><a href="/usuario/registro" title="Registro">Registro</a></li>
		<li><a href="/usuario/ingreso" title="Ingreso">Ingreso</a></li>
		<li><a href="/faqs" title="FAQS">FAQS</a></li>
		<li><a href="/contactanos" title="Contactanos">Contáctanos</a></li>
	</ul>
</div>
<?php } ?>