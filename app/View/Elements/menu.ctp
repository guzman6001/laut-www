﻿<center>
<?php echo $this->Html->link(
	$this->Html->image('icons/home.png', 
	array('alt'=> __('Home', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'sitio','action'=>'index'), 
	array('escape' => false));
?>


<?php echo $this->Html->link(
	$this->Html->image('icons/blog.png', 
	array('alt'=> __('Blog', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'posts','action'=>'index'), 
	array('escape' => false));
?>

<?php echo $this->Html->link(
	$this->Html->image('icons/nanoblog.png', 
	array('alt'=> __('Nanoblog', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'microposts','action'=>'index'), 
	array('escape' => false));
?>

<?php echo $this->Html->link(
	$this->Html->image('icons/links.png', 
	array('alt'=> __('Links', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'links','action'=>'index'), 
	array('escape' => false));
?>

<?php echo $this->Html->link( 
	$this->Html->image('icons/search.png', 
	array('alt'=> __('Buscar', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'sitio','action'=>'buscar'), 
	array('escape' => false));
?>

<?php echo $this->Html->link(
	$this->Html->image('icons/login.png', 
	array('alt'=> __('Login', true), 'border' => '0','class'=>'img-menu')),
	array ('controller'=>'users','action'=>'login'), 
	array('escape' => false));
?></center>