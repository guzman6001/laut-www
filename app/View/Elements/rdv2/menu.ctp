﻿<?php 

$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<nav id="topnav" role="navigation">
	<div class="menu-toggle">Menú</div> 
	
	<ul class="srt-menu" id="menu-main-navigation">
<?php if($isok) { ?>
		<li><a href="/enlaces">Enlaces</a></li>
		<li><a href="/notas">Notas</a></li>
		<li><a href="/usuario/perfil/<?= AuthComponent::user('username') ?>"><?= AuthComponent::user('name') ?> <?= AuthComponent::user('lastname') ?></a>
			<ul>
				<li><a href="/usuario/editar_perfil">Editar Perfil</a></li>
				<li><a href="/usuario/perfil/<?= AuthComponent::user('username') ?>">Ver Perfil</a></li>
				<!--li><a href="/mensajes">Mensajes</a></li-->
			</ul>
		</li>
		<li><a href="/usuario/salir">Salir</a></li>
<?php }else{ ?>
<li><a href="/usuario/registro">Registrarse</a></li>
<li><a href="/usuario/acceder">Acceder</a></li>
<?php } ?>		
	</ul>
</nav><!-- end main navigation -->