<?php $this->Html->addCrumb('Inicio','/sitio'); ?>

<div class="grid_11">
<center>
</center>
</div>


<div>
<center>

<div class="grid_5"><!-- CONTENEDOR IZQUIERDO -->



<!-- COMICS -->
<table class="tablita completa">
<tr><th><?php echo __('Dibujos / Tiras / Comics'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'comics','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td class="actions">
<?php if(empty($comics)): ?>
La tira no está disponible por el momento.
<?php else:
foreach ($comics as $comic): ?>
<center><img 
	alt="<?php echo $comic['Comic']['title']; ?>"
	title="<?php echo $comic['Comic']['title']; ?>"
	style="max-width:380px;"
	src="<?php echo ($comic['Comic']['file']); ?>" />
</center><br />
<?php endforeach; ?>
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'comics','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>








<!-- REDES SOCIALES -->
<table class="tablita completa">
<tr><th>Redes Sociales, Páginas y Perfiles Públicos</th></tr>
<tr><td><center>
<?php echo $this->Html->link(
	$this->Html->image('icons/facebook.png', 
	array('alt'=> __('Facebook', true), 'border' => '0','class'=>'img-sn')),
	'https://www.facebook.com/guzman6001page', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/twitter.png', 
	array('alt'=> __('Twitter', true), 'border' => '0','class'=>'img-sn')),
	'http://twitter.com/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/identica.png', 
	array('alt'=> __('Identi.ca', true), 'border' => '0','class'=>'img-sn')),
	'http://identi.ca/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/linkedin.png', 
	array('alt'=> __('LinkedIn', true), 'border' => '0','class'=>'img-sn')),
	'http://ve.linkedin.com/pub/héctor-guzmán/32/341/555', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/tumblr.png', 
	array('alt'=> __('Tumblr', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.tumblr.com', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/wpblog.png', 
	array('alt'=> __('Blog en Wordpress', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.wordpress.com/', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/stripgenerator.png', 
	array('alt'=> __('Stripgenerator', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.stripgenerator.com/gallery', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/pixton.png', 
	array('alt'=> __('Pixton', true), 'border' => '0','class'=>'img-sn')),
	'http://www.pixton.com/es/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/deviant.png', 
	array('alt'=> __('Deviant', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.deviantart.com/', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/subcultura.png', 
	array('alt'=> __('Subcultura', true), 'border' => '0','class'=>'img-sn')),
	'http://subcultura.es/user/guzman6001/', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/goear.png',  
	array('alt'=> __('Goear', true), 'border' => '0','class'=>'img-sn')),
	'http://www.goear.com/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/jamendo.png',  
	array('alt'=> __('Jamendo', true), 'border' => '0','class'=>'img-sn')),
	'http://www.jamendo.com/es/user/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/dropbox.png',  
	array('alt'=> __('Dropbox', true), 'border' => '0','class'=>'img-sn')),
	'http://db.tt/gkipuKv', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/imageshack.png', 
	array('alt'=> __('Imageshack', true), 'border' => '0','class'=>'img-sn')),
	'http://imageshack.us/homepage/?user=guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>
</center></td></tr></table>
<!-- TWITTER -->
<table class="tablita completa"><tr><td>
<center><script charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 10,
  interval: 30000,
  width: 'auto',
  height: 400,
  theme: {
    shell: {
      background: '#009fc7',
      color: '#ffffff'
    },
    tweets: {
      background: '#f0f0f0',
      color: '#000000',
      links: '#000970'
    }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: true,
    behavior: 'all'
  }
}).render().setUser('guzman6001').start();
</script>
<br />
<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fguzman6001page&amp;width=300&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=true&amp;appId=27212646371" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:290px;" allowTransparency="true"></iframe></center>
</td></tr></table>




<!-- NANOBLOG -->
<table class="tablita completa alternada" style="clear: left;">
<tr><th><?php echo __('Nanoblog'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'microposts','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<?php if(empty($microposts)): ?>
<tr><td>No hay microposts guardados por el momento.</td></tr>
<?php else:
foreach ($microposts as $micropost): ?>
<tr><td>
		<?php echo $micropost['Micropost']['mpbody'] ?>
</td></tr>
<?php endforeach; ?><br />
<tr><td class="actions"><center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'microposts','action' => 'index'));?></center>
</td></tr>
<?php endif; ?>
</table>



<!-- ENLACES -->
<table class="tablita completa alternada" style="clear: left;">
<tr><th><?php echo __('Enlaces'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'links','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<?php if(empty($links)): ?>

<tr><td>No hay links guardados por el momento.</td></tr>

<?php else:
foreach ($links as $link): ?>
	<tr><td>	
	<a	title="<?php echo $link['Link']['description'] ?>"
		target='_blank'
		href="<?php echo $link['Link']['url'] ?>">
			<?php echo $link['Link']['title'] ?>
	</a>
	</td></tr>
<?php endforeach; ?><br />
<tr><td class="actions">
	<center>
		<?php echo $this->Html->link(__('View more >>'), 
			array('controller' => 'links','action' => 'index'));?>
	</center>
</td></tr>
<?php endif; ?>
</table>











</div>
<div class="grid_6"><!-- CONTENEDOR DERECHO -->


<!-- FOTOS -->
<table class="tablita completa">
<tr><th><?php echo __('Fotografías'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'fotos','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td class="actions">
<?php if(empty($fotos)): ?>
La foto no está disponible por el momento.
<?php else:
foreach ($fotos as $foto): ?>
<center><img 
	alt="<?php echo $foto['Foto']['title']; ?>"
	title="<?php echo $foto['Foto']['title']; ?>"
	style="max-width:380px;"
	src="<?php echo ($foto['Foto']['file']); ?>" />
</center><br />
<?php endforeach; ?>
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'fotos','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>





<!-- BLOG -->
<table class="tablita completa alternada" style="clear: left;">
<tr><th><?php echo __('Blog'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'posts','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<?php if(empty($posts)): ?>

<tr><td>No hay posts guardados por el momento.</td></tr>

<?php else:
foreach ($posts as $post): ?>
	<tr><td>
	&nbsp;&nbsp;&nbsp;
	<a	title="<?php echo $post['Post']['description'] ?>"
		href="posts/view/<?php echo $post['Post']['id'] ?>/<?php echo $post['Post']['slug'] ?>">
			<?php echo $post['Post']['title'] ?>
	</a>
</td></tr>
<?php endforeach; ?>

<tr><td class="actions"><center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'posts','action' => 'index'));?></center></td></tr>
	
<?php endif; ?>
</td></tr></table>




<!-- VIDEOS -->
<table class="tablita completa">
<tr><th><?php echo __('Videos'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'videos','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td class="actions">
<?php if(empty($videos)): ?>
No hay videos guardados por el momento.
<?php else:

foreach ($videos as $video): ?>
	<div>
		<iframe width="100%" height="315" 
		src="http://www.youtube.com/embed/<?php echo $video['Video']['code']; ?>?rel=0" 
		frameborder="0"></iframe>

	</div>
<?php endforeach; ?><br />
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'videos','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>




<!-- IMAGENES -->
<table class="tablita completa">
<tr><th><?php echo __('Imágenes'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'images','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td class="actions">
<?php if(empty($images)): ?>
No hay videos guardados por el momento.
<?php else:
foreach ($images as $image): ?>
<center><img 
	alt="<?php echo $image['Image']['title']; ?>"
	title="<?php echo $image['Image']['title']; ?>"
	style="max-width:380px;"	
	src="<?php echo ($image['Image']['file']); ?>" /></center>

<?php endforeach; ?>
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'images','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>

















</div>
</center>
</div>



