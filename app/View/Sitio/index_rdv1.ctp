
<div id="main">

    <!-- Start Slider Wrapping>
    <div id="sliderwrap">
		
        < Start Slider>
        <div id="slider" class="nivoSlider">
            <img src="/img/rdv1/slider-banners/slider01.jpg" alt=""/>
            <img src="/img/rdv1/slider-banners/slider02.jpg" alt=""/>
            <img src="/img/rdv1/slider-banners/slider03.jpg" alt=""/>
        </div>
        <End Slider -->
        <!-- Start Slider HTML Captions>
        <div id="htmlcaption" class="nivo-html-caption">
            <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>.
        </div>
        < End Slider HTML Captions>
    
    </div>
    < End Slider Wrapping -->
    <!-- Start H1 Title -->
    <div class="titles">
    	<h1>Explora las opciones de LAUT.com.ve para mejorar tu productividad</h1>
        <span></span>
    </div>
    <!-- End H1 Title -->
    <!-- Start Main Body Wrap -->
    <div id="main-wrap">
    	<!-- Start Featured Boxes -->
        <div class="boxes-third boxes-first">
        	<div class="boxes-padding">
            	<div class="bti">
                	<div class="featured-images"><img src="/img/rdv1/responsive-icon.png" width="72" height="53" alt="Enlaces"></div>
                	<div class="featured-titles">Enlaces</div>
                </div>
                <div class="featured-text">Guarda todos tus enlaces y recuperalos fácilmente.</div>
            </div>
            <span class="box-arrow"></span>
        </div>
        
        <div class="boxes-third">
        	<div class="boxes-padding">
            	<div class="bti">
                    <div class="featured-images"><img src="/img/rdv1/cleansleek-icon.png" width="66" height="53" alt="To Do List"></div>
                    <div class="featured-titles">Lista de tareas</div>
                </div>
                <div class="featured-text">Maneja tu lista de tareas (to do list) de manera sencilla.</div>
            </div>
            <span class="box-arrow"></span>
        </div>
        
        <div class="boxes-third boxes-last">
        	<div class="boxes-padding">
                <div class="bti">
                    <div class="featured-images"><img src="/img/rdv1/google-icon.png" width="54" height="53" alt="Notas"></div>
                    <div class="featured-titles">Notas</div>
                </div>
                <div class="featured-text">Crea tu propio cuaderno de anotaciones, comparte tus publicaciones con tus colegas, de manera pública o mantenlas para tu uso personal.</div>
            
            </div>
            
            <span class="box-arrow"></span>
        
        </div>
        <!-- End Featured Boxes -->
    </div>
    <!-- End Main Body Wrap -->

</div>