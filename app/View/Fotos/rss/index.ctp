<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Fotografías"),
    	'link' => $this->Html->link(__('Fotos'), array('controller' => 'fotos', 'action' => 'index.rss')),
    	'description' => __("Fotos recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($fotos as $post) 
{
	if ($post['Foto']['rss'])
	{
    		$postTime = strtotime($post['Foto']['created']);
		$bodyText=$post['Foto']['description'];
    		$postLink = '/fotos/view/'.$post['Foto']['id']."/".$post['Foto']['slug'];

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Foto']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Foto']['created']
    		));
	}
}
