<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Fotos', '/fotos');
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_9">
	<center><h2><?php echo __('Fotos');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'fotos','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr /></center>
<?php
	foreach ($fotos as $foto):
?>
<center><table class="tablita">
<tr>
	<th><?php echo ($foto['Foto']['title']); ?></th>
</tr>
<tr><td><center><img 
	alt="<?php echo $foto['Foto']['title']; ?>"
	title="<?php echo $foto['Foto']['title']; ?>"
	style="max-width: 680px;"	
	src="<?php echo ($foto['Foto']['file']); ?>" /></center>
</td>
</tr>
<tr>
	<td><center>
	Álbum: <?php echo $this->Html->link($foto['Foto']['album'], array('action' => 'album', $foto['Foto']['albumslug']));?></center>
	</td>
</tr>
<tr>
	<td class="actions"><center>
	<?php echo $this->Html->link(__('Comentar / Compartir / Ver detalles'), array('action' => 'view', $foto['Foto']['id'],$foto['Foto']['slug']));?>
				
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $foto['Foto']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $foto['Foto']['id']), null, __('Are you sure you want to delete # %s?'), $foto['Foto']['id']); ?>
	<?php }?>
	</center></td>
</tr>
</table></center>
<?php endforeach; ?>
<center><?php echo $this->element('paginator'); ?></center>
</div>

<div class="grid_2">
	<?php echo $this->element('publicidad_vertical'); ?>
</div>