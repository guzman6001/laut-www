<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Fotos', '/fotos'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_11"><center>
<h2><?php echo ($foto['Foto']['title']) ;?></h2><hr />
<table class="tablita completa">
<tr><td><center><img 
	alt="<?php echo $foto['Foto']['title']; ?>"
	title="<?php echo $foto['Foto']['title']; ?>"
	style="max-width: 680px;"	
	src="<?php echo ($foto['Foto']['file']); ?>" /></center>
</td>
</tr>
<tr><td><center><?php echo ($foto['Foto']['description']); ?></center></td></tr>
<tr>
	<td><center>
	Álbum: <?php echo $this->Html->link($foto['Foto']['album'], array('action' => 'album', $foto['Foto']['albumslug']));?></center>
	</td>
</tr>
<tr>
	<td class="actions"><center>	
		<a target="_blank" 
			href="<?php echo $foto['Foto']['file']; ?>">Tamaño Completo</a> 
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $foto['Foto']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $foto['Foto']['id']), null, __('Are you sure you want to delete # %s?'), $foto['Foto']['id']); ?>
	<?php }?>
	</center></td>
</tr>
<tr><td>
	<?php echo $this->element('social'); ?>
</td></tr>
</table>
</center></div>
