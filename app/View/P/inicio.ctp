﻿<div id="tope"><img src="/files/rdv2/images/cargando.gif" /><br />CARGANDO</div>
<!-- main content area -->   
<div id="main" class="wrapper">
    
    
<!-- content area -->    
	<section id="content" class="wide-content">
    	<div class="grid_4">
        	<center><h1 class="first-header">Enlaces</h1></center>
            <div id="accordian">
			<ul>
				<?php 
				if(empty($enlacecategories))
				{echo"<li><h3>No se han guardado categorias todavía</h3></li>";}
				else
				{ 
				
					foreach ($enlacecategories as $category): 
					if (!empty($category['Enlace']))
					{					?>
					<li>
					<h3><?php echo ($category['Category']['name']); ?></h3>
					<ul>
						<?php 
						
						foreach ($category['Enlace'] as $link): 
						
						?>
						<li>
						<a title="<?php echo $link['title'] ?>"
						
						href="/enlaces/ver/<?php echo $link['id'] ?>">
							<?php echo $link['title'] ?>
						</a>
						</li>			
						<?php endforeach; ?>
						
					</ul>
					</li>
					<?php 
					}endforeach; ?>	
				<?php 
				} ?>
			</ul>
			</div>
        </div>
        
		<div class="grid_4">
			<center><h1 class="first-header">Notas</h1></center>
            <div id="accordian">
			<ul>
				<?php 
				if(empty($enlacecategories))
				{echo"<li><h3>No se han guardado categorias todavía</h3></li>";}
				else
				{ 
				
					foreach ($enlacecategories as $category): 
					if (!empty($category['Nota']))
					{					?>
					<li>
					<h3><?php echo ($category['Category']['name']); ?></h3>
					<ul>
						<?php 
						
						foreach ($category['Nota'] as $link): 
						
						?>
						<li>
						<a title="<?php echo $link['title'] ?>"
	
						href="/notas/ver/<?php echo $link['id'] ?>/<?php echo $link['slug'] ?>">
							<?php echo $link['title'] ?>
						</a>
						</li>			
						<?php endforeach; ?>
						
					</ul>
					</li>
					<?php 
					}endforeach; ?>	
				<?php 
				} ?>
			</ul>
			</div>
		</div>
		
		
        <div class="grid_4">
        	<center><h1 class="first-header">Recientes</h1></center>
            <?php if (empty($enlaces))
			{
				echo ('<center>- No has generado contenido aun -</center>'); 
			} 
			else 
			{
				echo '<div class="cuadro_blanco"><div><h3>Enlaces</h3><hr /><ul>';
				
				foreach ($enlaces as $enlace): 
					echo "<li><a style='font-size:small' href='/enlaces/ver/".$enlace['Enlace']['id']."'>".$enlace['Enlace']['title']."</a></li>";
					?>
			<?php	
				endforeach;
			}?>
			</ul></div></div>
			
			 <?php if (empty($notas))
			{
				echo ('<center>- No has generado contenido aun -</center>'); 
			} 
			else 
			{
				echo '<div class="cuadro_blanco"><div><h3>Notas</h3><hr /><ul>';
				
				foreach ($notas as $enlace): 
					echo "<li><a style='font-size:small' href='/notas/ver/".$enlace['Nota']['id']."/".$enlace['Nota']['slug']."'>".$enlace['Nota']['title']."</a></li>";
					?>
			<?php	
				endforeach;
			}?>
			</ul></div></div>
			
        </div>

	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->