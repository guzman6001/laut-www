﻿<div id="tope"><img src="/files/rdv2/images/cargando.gif" /><br />CARGANDO</div>
<?php 
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){header("Location:/p/inicio"); exit;}
}
?>
 
<!-- hero area (the grey one with a slider -->
    <section id="hero" class="clearfix">    
    <!-- responsive FlexSlider image slideshow -->
    <div class="wrapper">
        <div class="grid_5 alpha">
                <h1>Laut</h1>
            <p align="justify">
            <strong>Laut </strong> es una herramienta que te ayudará a organizar tu tiempo y esfuerzo eficientemente, para que logres realizar tus proyectos de una manera sencilla. Mezcla lo mejor de los sistemas operativos en línea con aspectos sociales que permiten la creación de comunidades virtuales, la generación de conocimientos, ls compartición de información y la publicación de materiales profesionales. Laut está en etapa de desarrollo y crecimiento, por lo que su imagen y funciones pueden variar sin previo aviso.  
            </p>
            <p style="text-align: center !important;">
			<a href="/usuario/registro" class="buttonlink">Registrarse</a>
			<a href="/usuario/acceder" class="buttonlink">Acceder</a>
			</p>
        </div>
        <div class="grid_7 omega rightfloat">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <a href="/enlaces"><img src="/files/rdv2/images/enlaces.jpg" /></a>
                            <p class="flex-caption">Guarda y accede a tus <strong>enlaces</strong> fácilmente.</p>
                        </li>
						
						<li>
                            <a href="/tareas"><img src="/files/rdv2/images/tareas.jpg" /></a>
                            <p class="flex-caption">Organiza tus actividades con una lista de <strong>tareas</strong>.</p>
                        </li>
						
						<li>
                            <a href="/notas"><img src="/files/rdv2/images/notas.jpg" /></a>
                            <p class="flex-caption">Crea contenido a través de <strong>notas</strong>. Puedes compartirlas o guardarlas para ti.</p>
                        </li>
                    </ul>
                  </div>
                </div><!-- FlexSlider -->
        </div>
    </section><!-- end hero area -->

<!-- main content area -->   
<div id="main" class="wrapper">
    
    
<!-- content area -->    
	<section id="content" class="wide-content">
    	<div class="grid_4">
        	<h1 class="first-header">Enlaces</h1>
            <img src="/files/rdv2/images/enlaces.jpg" />
            <p align="justify">¿Cuáles son tus opciones cuando quieres guardar o compartir un enlace? ¿Lo envías por correo, lo publicas en redes sociales, lo guardas en documentos? ¿Los tienes organizados? ¿Puedes acceder a ellos cuando quieras? ¿Dónde están todos los enlaces que has compartido? Con Laut puedes guardar y compartir tus enlaces o vínculos hacia otras páginas web, organizarlos y encontrarlos rápidamente.</p>
        </div>
        
        <div class="grid_4">
        	<h1 class="first-header">Notas</h1>
            <img src="/files/rdv2/images/notas.jpg" />
            <p align="justify">Crea tu propio cuaderno de notas, comparte las anotaciones que tu decidas o crea tu blog personal. Puedes incluso crear páginas con tus amigos y compañeros para generar conocimiento. Al momento de llevar a cabo grandes proyectos, las notas son importante para documentar o respaldar el trabajo de los participantes.</p>
        </div>
        
        <div class="grid_4">
        	<h1 class="first-header">Tareas</h1>
            <img src="/files/rdv2/images/tareas.jpg" />
            <p align="justify">Organiza tu trabajo o tu vida diaria a través de una lista de tareas. Dentro del trabajo en equipo Laut te permite asignarle tareas a tus compañeros, de tal modo que nunca olviden las cosas que te deben. Una lista de tarea bien concebida puede ayudarte a llevar a cabo grandes proyectos.</p>
        </div>

	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->