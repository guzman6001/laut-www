﻿<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Comics', '/comics');
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>
<div class="grid_1">&nbsp;</div>

<div class="grid_7">
	<center><h2><?php echo __('Comics');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'comics','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr /></center>
<?php
	foreach ($comics as $comic):
	/*$style = null;
	if ($i++ % 2 == 0) 
	{
		$style = 'style="clear: left;"';
	}*/ ?>

<center><table class="tablita">
<tr>
	<th><?php echo ($comic['Comic']['title']); ?></th>
</tr>
<tr><td><center><img 
	alt="<?php echo $comic['Comic']['title']; ?>"
	title="<?php echo $comic['Comic']['title']; ?>"
	style="max-width: 500px;"	
	src="<?php echo ($comic['Comic']['file']); ?>" /></center>
</td>
</tr>

<tr>
	<td class="actions"><center>
	<?php echo $this->Html->link(__('Ver detalles / Comentar / Compartir / Descargar'), array('action' => 'view', $comic['Comic']['id'],$comic['Comic']['slug']));?>		
				
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $comic['Comic']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $comic['Comic']['id']), null, __('Are you sure you want to delete # %s?', $comic['Comic']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
</table></center>
<?php endforeach; ?>	
<?php echo $this->element('paginator'); ?>

</div>

<div class="grid_1">&nbsp;</div>
<div class="grid_2">
	<?php echo $this->element('publicidad_vertical'); ?>
</div>