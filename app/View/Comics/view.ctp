﻿<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Comics', '/comic'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_11"><center>
<h2><?php echo ($comic['Comic']['title']) ;?></h2><hr />
<table class="tablita completa">
<tr><td><center><img 
	alt="<?php echo $comic['Comic']['title']; ?>"
	title="<?php echo $comic['Comic']['title']; ?>"
	style="max-width: 680px;"	
	src="<?php echo ($comic['Comic']['file']); ?>" /></center>
</td>
</tr>

<tr><td><center><?php echo ($comic['Comic']['description']); ?></center></td></tr>

<tr><td><center>
	<strong>Saga:</strong>
	<?php echo $this->Html->link(
		$comic['Comic']['saga'], 
		array('action' => 'saga', $comic['Comic']['sagaslug'])); ?> 
</center></td></tr>

<tr><td><center>
	<strong>Herramienta(s):</strong> 
	<?php echo $comic['Comic']['tools']; ?>. 
</center></td></tr>

<tr>
	<td class="actions"><center>		
		<a target="_blank" 
			href="<?php echo $comic['Comic']['file']; ?>">Tamaño Completo</a> 
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $comic['Comic']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $comic['Comic']['id']), null, __('Are you sure you want to delete # %s?', $comic['Comic']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
<tr><td>
	<?php echo $this->element('social'); ?>
</td></tr>
</table>
</center></div>
