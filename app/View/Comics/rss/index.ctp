<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Imágenes"),
    	'link' => $this->Html->link(__('Comics'), array('controller' => 'comics', 'action' => 'index.rss')),
    	'description' => __("Comics recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($comics as $post) 
{
	if ($post['Comic']['rss'])
	{
    		$postTime = strtotime($post['Comic']['created']);
		$bodyText=$post['Comic']['description'];
    		$postLink = '/comics/view/'.$post['Comic']['id']."/".$post['Comic']['slug'];

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Comic']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Comic']['created']
    		));
	}
}
