<!-- main content area -->   
<div id="main" class="wrapper">
    
    
<!-- content area -->    
	<section id="content" class="wide-content">
    	<div class="grid_6">
        	<h1 class="first-header">Enviar</h1>
            <div class="formulario">
			<?php 
			
			echo $this->Form->create('Mensaje', array('url' => '/mensajes/enviar'));?>
				<fieldset>
					<legend><?php echo __('Enviar mensaje'); ?></legend>
				<?php
					//echo $this->Form->input('title');
					echo $this->Form->input('recipient_id');
					echo $this->Form->input('subject', array('required' => true));
					echo $this->Form->input('description', array('required' => true));
				?>
				</fieldset>
			<?php echo $this->Form->end(__('Enviar'));
			?>
			</div>
        </div>
        
        <div class="grid_6">
        	<h1 class="first-header">Mensajes recibidos</h1>
            <?php if (empty($lations))
			{
				echo ('<center>- No han recibido mensajes aun -</center>'); 
			} 
			else 
			{
				foreach ($mensajes as $mensaje): 
			?>	
			
				
				<div class="enlace_seleccionado">
				<?php
					if ($mensaje['Mensaje']['name']=="Enlace")
					{
						echo "<h3><span style='color:#125c00;' target='_blank' href='".$mensaje['Mensaje']['url']."'>".$lation['Lation']['title']."</span></h3>";
					}
					else
					{
						"<h3>".$Mensaje['Mensaje']['subject']."</h3>";
					}
				?>
					<?php echo($Mensaje['Mensaje']['description']); ?>
				</div>
			<?php	
				
				endforeach;
			}?>
        </div>

	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->