<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Música Libre"),
    	'link' => $this->Html->link(__('Canciones'), array('controller' => 'songs', 'action' => 'index.rss')),
    	'description' => __("Canciones recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($songs as $post) 
{
	if ($post['Song']['rss'])
	{
		$postTime = strtotime($post['Song']['created']);
		$bodyText=$post['Song']['title']." - ".$post['Song']['artist'];
		$postLink = '/songs/view/'.$post['Song']['id'];

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Song']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Song']['created']
    		));
	}
}
