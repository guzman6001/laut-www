﻿<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Música Libre', '/songs'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<link href="/css/prettify-jPlayer.css" rel="stylesheet" type="text/css" />
<link href="/css/circle/circle.player.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="/js/jquery.jplayer.inspector.js"></script>
<script type="text/javascript" src="/js/jquery.transform.js"></script>
<script type="text/javascript" src="/js/jquery.grab.js"></script>
<script type="text/javascript" src="/js/mod.csstransforms.min.js"></script>
<script type="text/javascript" src="/js/circle.player.js"></script>

<script type="text/javascript">
//<![CDATA[
$(document).ready(function()
{
	var	opt_play_first = false, opt_auto_play = true;
	var myCirclePlayer = new CirclePlayer("#jquery_jplayer_1",
	{
		mp3: "<?php echo ($song['Song']['file']); ?>"
	}, 
	{
		cssSelectorAncestor: "#cp_container_1",
		swfPath: "/js",
		wmode: "window"
	});
});
//]]>
</script>

<div class="grid_11"><center>
<h2><?php echo ($song['Song']['title']); ?></h2><hr />
<table class="tablita completa">
<tr><td>

<center>
<table class="tablita">
<tr><th>NOTA LEGAL</th></tr>
<tr><td>Las canciones en esta página son de licencia libre, lo que quiere decir que puedes descargarlas, compartirlas y escucharlas libremente, ya que cuentas de antemano con el permiso de sus autores. Lee más en la página de <a href="http://www.jamendo.com/es/creativecommons" target="_blank">Jamendo</a>.</td></tr>
</table>

<div id="jquery_jplayer_1" class="cp-jplayer"></div>

			<!-- The container for the interface can go where you want to display it. Show and hide it as you need. -->

			<div id="cp_container_1" class="cp-container">
				<div class="cp-buffer-holder"> <!-- .cp-gt50 only needed when buffer is > than 50% -->
					<div class="cp-buffer-1"></div>
					<div class="cp-buffer-2"></div>
				</div>
				<div class="cp-progress-holder"> <!-- .cp-gt50 only needed when progress is > than 50% -->
					<div class="cp-progress-1"></div>
					<div class="cp-progress-2"></div>
				</div>
				<div class="cp-circle-control"></div>
				<ul class="cp-controls">
					<li><a href="#" class="cp-play" tabindex="1">play</a></li>
					<li><a href="#" class="cp-pause" style="display:none;" tabindex="1">pause</a></li> <!-- Needs the inline style here, or jQuery.show() uses display:inline instead of display:block -->
				</ul>
			</div>
			
			
			
			
</center>
</td></tr>
<tr><td><center>Por: <?php echo ($song['Song']['artist']); ?></center></td></tr>
<tr>
	<td class="actions"><center>				
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit Song'), array('action' => 'edit', $song['Song']['id'])); ?>
        	<?php echo $this->Form->postLink(__('Delete Song'), array('action' => 'delete', $song['Song']['id']), null, __('Are you sure you want to delete # %s?', $song['Song']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
</table>


</center>
</div>
