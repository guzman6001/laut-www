﻿<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Música Libre', '/songs');
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>
<link href="/css/prettify-jPlayer.css" rel="stylesheet" type="text/css" />
<link href="/css/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="/js/jplayer.playlist.min.js"></script>
<script type="text/javascript" src="/js/jquery.jplayer.inspector.js"></script>

<script type="text/javascript">
//<![CDATA[
$(document).ready(function()
{
	new jPlayerPlaylist(
	{
		jPlayer: "#jquery_jplayer_1",
		cssSelectorAncestor: "#jp_container_1"
	}, [
	{
			title:"Nota Legal. By: Soledad.",mp3:"http://dl.dropbox.com/u/26600853/music/nota-legal-soledad.mp3"
	},
	<?php foreach ($songs as $song): ?>
	{
		title:"<?php echo h($song['Song']['title']); ?>. By: <?php echo h($song['Song']['artist']); ?>.",
		mp3:"<?php echo h($song['Song']['file']); ?>"
	},
	<?php endforeach; ?>
	{
			title:"Nota Legal. By: Carlos.",			mp3:"http://dl.dropbox.com/u/26600853/music/nota-legal-carlos.mp3"
	}

	], 
	{

		swfPath: "/js",
		supplied: "oga, mp3",
		wmode: "window"
	});
	$("#jplayer_inspector_1").jPlayerInspector({jPlayer:$("#jquery_jplayer_1")});

});

//]]>
</script>


<div class="grid_11">
	<center><h2><?php echo __('Música Libre');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'songs','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr /></center></h2><hr /></center>
</div>

<div class="grid_7">
<center>
<table class="tablita">
<tr><th>NOTA LEGAL</th></tr>
<tr><td>Las canciones en esta página son de licencia libre, 
lo que quiere decir que puedes descargarlas, compartirlas 
y escucharlas libremente, ya que cuentas de antemano con 
el permiso de sus autores. Lee más en la página de 
<a href="http://www.jamendo.com/es/creativecommons" target="_blank">Jamendo</a>.
</td></tr>
</table>
<div id="jquery_jplayer_1" class="jp-jplayer"></div>
<div id="jp_container_1" class="jp-audio" style="text-align:left !important;">
	<div class="jp-type-playlist">
		<div id="jp_interface_1" class="jp-gui jp-interface">
			<ul class="jp-controls">
				<li><a href="javascript:;" class="jp-previous" tabindex="1">previous</a></li>
				<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
				<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
				<li><a href="javascript:;" class="jp-next" tabindex="1">next</a></li>
				<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
				<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
				<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
				<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
			</ul>
			
			<div class="jp-progress">
				<div class="jp-seek-bar">
					<div class="jp-play-bar"></div>
				</div>
			</div>

			<div class="jp-volume-bar">
				<div class="jp-volume-bar-value"></div>
			</div>

			<div class="jp-time-holder">
				<div class="jp-current-time"></div>
				<div class="jp-duration"></div>
			</div>

			<ul class="jp-toggles">
				<li><a href="javascript:;" class="jp-shuffle" tabindex="1" title="shuffle">shuffle</a></li>
				<li><a href="javascript:;" class="jp-shuffle-off" tabindex="1" title="shuffle off">shuffle off</a></li>
				<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
				<li><a href="javascript:;" class="jp-repeat-on" tabindex="1" title="repeat off">repeat off</a></li>
			</ul>
		</div>

		<div class="jp-playlist">
			<ul>
				<li></li>
			</ul>
		</div>

		<div class="jp-no-solution">
			<span>Update Required</span>
			To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
		</div>
	</div>
</div>
</center>
</div>

<div class="grid_4">
<center>
<table class="tablita">
<tr><th>
Nota de Calidad
</th></tr>
<tr><td>Esta funcionalidad está en etapa de contrucción 
y pruebas por lo que es posible que experimente 
mal-funcionamientos exporádicos.</td></tr>
</table>
<br /><br /><br />
<table class="tablita alternada">
<tr><th>¿Qué quieres escuchar?</th></tr>

	<?php foreach ($sings as $cancion): ?>
	<tr><td>
	<a href="/songs/playlist/<?php echo $cancion['Song']['playlistslug']; ?>">
		<?php echo $cancion['Song']['playlist']; ?>
	</a> 
	</td></tr>
<?php endforeach; ?> 
</table>
</center>
</div>
<?php echo $this->element('paginator'); ?>

