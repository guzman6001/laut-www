﻿<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Música', '/songs');
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_11">
	<center><h2><?php echo __('Música');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'songs','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr /></center></h2><hr /></center>




<center><table class="tablita">
	<tr><th colspan="3">Lista de Canciones&nbsp;</th></tr>
	<tr>
		<td><strong>Título</strong></td>
		<td><strong>Artista</strong></td>
		<td><strong>Acciones</strong></td>
	</tr>
<?php foreach ($songs as $song): ?>
	<tr>
		<td><?php echo h($song['Song']['title']); ?></td>
		<td><?php echo ($song['Song']['artist']); ?></td>
		<td class="actions">
		<?php echo $this->Html->link(__('Circle'), array('action' => 'viewcircle', $song['Song']['id'])); ?>
		<?php echo $this->Html->link(__('View'), array('action' => 'view', $song['Song']['id'])); ?>
			<?php if ($isok){ ?>        
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $song['Song']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), 
			array('action' => 'delete', $song['Song']['id']), 
		array('title' => 'Eliminar'), 
		__('Are you sure you want to delete # %s?', $song['Song']['id']));}?>
		</td>	
</tr>
<?php endforeach; ?>	
</table>
</center>

<?php echo $this->element('paginator'); ?>
</div>
