﻿<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Música Libre', '/songs'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<link href="/css/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery.jplayer.min.js"></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function()
{
	var opt_auto_play = true;
	$("#jquery_jplayer_1").jPlayer({
		ready: function (event) {
			$(this).jPlayer("setMedia", {
				mp3:"<?php echo ($song['Song']['file']); ?>"
			}).jPlayer("play");
		},
		swfPath: "/js",
		supplied: "mp3",
		wmode: "window"
	});
});
//]]>
</script>


<div class="grid_11"><center>
<h2><?php echo ($song['Song']['title']); ?></h2><hr />
<table class="tablita completa">
<tr><td>

<center>
<table class="tablita">
<tr><th>NOTA LEGAL</th></tr>
<tr><td>Las canciones en esta página son de licencia libre, lo que quiere decir que puedes descargarlas, compartirlas y escucharlas libremente, ya que cuentas de antemano con el permiso de sus autores. Lee más en la página de <a href="http://www.jamendo.com/es/creativecommons" target="_blank">Jamendo</a>.</td></tr>
</table>

<div id="jquery_jplayer_1" class="jp-jplayer"></div>
	<div id="jp_container_1" class="jp-audio">
		<div class="jp-type-single">
			<div class="jp-gui jp-interface">
				<ul class="jp-controls">
					<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
					<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
					<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
					<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
					<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
					<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
				</ul>
				<div class="jp-progress">
					<div class="jp-seek-bar">
						<div class="jp-play-bar"></div>
					</div>
				</div>
				<div class="jp-volume-bar">
					<div class="jp-volume-bar-value"></div>
				</div>
				<div class="jp-time-holder">
					<div class="jp-current-time"></div>
					<div class="jp-duration"></div>

					<ul class="jp-toggles">
						<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
						<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
					</ul>
				</div>
			</div>
			<div class="jp-title">
				<ul>
					<li><?php echo ($song['Song']['title']); ?></li>
				</ul>
			</div>
			<div class="jp-no-solution">
				<span>Update Required</span>
				To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
			</div>
		</div>
	</div>
			
			
			
			
</center>
</td></tr>
<tr><td><center>Por: <?php echo ($song['Song']['artist']); ?></center></td></tr>
<tr>
	<td class="actions"><center>
		<a href="/songs/playlist/<?php echo $song['Song']['playlistslug']; ?>">Otras tipo <?php echo $song['Song']['playlist']; ?></a>
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit Song'), array('action' => 'edit', $song['Song']['id'])); ?>
        	<?php echo $this->Form->postLink(__('Delete Song'), array('action' => 'delete', $song['Song']['id']), null, __('Are you sure you want to delete # %s?', $song['Song']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
</table> 

<?php echo $this->element('social'); ?>
</center>
</div>
