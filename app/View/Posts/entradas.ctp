﻿<link href="/shl/styles/shThemeDefault.css" rel="stylesheet" type="text/css" />
<script src="/shl/scripts/shCore.js" type="text/javascript"></script>
<script src="/shl/scripts/shAutoloader.js" type="text/javascript"></script>
<script type="text/javascript" src="/shl/scripts/shBrushSql.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushBash.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushCpp.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushCss.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushJava.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushPhp.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushXml.js"></script>

<script type="text/javascript">SyntaxHighlighter.all();</script>


<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Blog', '/posts');
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_9 container">
	<center><h2><?php echo __('Blog');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'posts','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr /></center></h2><hr /></center>

<center><table class="tablita alternada">
<tr><th><?php echo "Todas las entradas"; ?></th></tr>
<?php
	foreach ($posts as $post): 
 ?>
<tr><td>
	<?php echo $this->Html->link(__($post['Post']['title']), array('action' => 'view', $post['Post']['id'],$post['Post']['slug'])); ?>
	
	<?php if ($isok){ ?>        
     	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $post['Post']['id'])); ?>
	 <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $post['Post']['id']), null, __('Are you sure you want to delete # %s?', $post['Post']['id']));}?>
</td></tr>

<?php endforeach; ?>
</table>
</center>
</div>

<div class="grid_2">
	<?php echo $this->element('publicidad_vertical'); ?>
</div>