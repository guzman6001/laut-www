<link href="/shl/styles/shThemeDefault.css" rel="stylesheet" type="text/css" />
<script src="/shl/scripts/shCore.js" type="text/javascript"></script>
<script src="/shl/scripts/shAutoloader.js" type="text/javascript"></script>
<script type="text/javascript" src="/shl/scripts/shBrushSql.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushBash.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushCpp.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushCss.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushJava.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushPhp.js"></script>
<script type="text/javascript" src="/shl/scripts/shBrushXml.js"></script>
<script type="text/javascript">SyntaxHighlighter.all();</script> 

<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Blog', '/posts'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>
<div class="grid_1">&nbsp;</div>
<div class="grid_9"><center>
<h2><?php echo h($post['Post']['title']); ?></h2><hr />
<table class="tablita completa">
<tr><td><?php echo ($post['Post']['pbody']); ?></td></tr>
<tr><td><center><?php echo h($post['Post']['created']); ?></center></td></tr>
<tr>
	<td class="actions"><center>				
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit Post'), array('action' => 'edit', $post['Post']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete Post'), array('action' => 'delete', $post['Post']['id']), null, __('Are you sure you want to delete # %s?', $post['Post']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
<tr><td>
 <?php echo $this->element('social'); ?>
</center></td></tr>
</table>
</center></div>
<div class="grid_1">&nbsp;</div>
