<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Blog"),
    	'link' => $this->Html->link(__('Blog'), array('controller' => 'posts', 'action' => 'index.rss')),
    	'description' => __("Posts recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($posts as $post) 
{
	if ($post['Post']['rss'])
	{
    		$postTime = strtotime($post['Post']['created']);
		$bodyText=$post['Post']['description'];
    		$postLink = '/posts/view/'.$post['Post']['id']."/".$post['Post']['slug'];

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Post']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Post']['created']
    		));
	}
}
