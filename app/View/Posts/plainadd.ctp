<?php $this->Html->addCrumb('Inicio', '/sitio'); ?>
<?php $this->Html->addCrumb('Blog', '/posts'); ?>

<div class="grid_1">&nbsp;</div>

<div class="grid_9">
<div class="formulario">
<?php echo $this->Form->create('Post');?>
	<fieldset>
		<legend><?php echo __('Add Post'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('slug');
		echo $this->Form->input('excerpt');
		echo $this->Form->input('description');
		echo $this->Form->input('pbody', array('rows' => '30'));
		echo $this->Form->input('rss');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>

<div class="grid_1">&nbsp;</div>
