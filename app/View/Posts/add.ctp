<?php $this->Html->addCrumb('Inicio', '/sitio'); ?>
<?php $this->Html->addCrumb('Blog', '/posts'); ?>

<script type="text/javascript"> 
    tinyMCE.init({ 
        theme : "advanced", 
	skin: "o2k7",
        mode : "exact",
        elements : "PostPbody",
        plugins: "codesyntax",  
		preformatted : true,
        convert_urls : false, 
		
		 // Theme options  
        theme_advanced_buttons1: "codeformat,|,code,|,cut,copy,paste,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull",
		theme_advanced_buttons2: "bullist,numlist,outdent,indent,|,iespell,link,unlink,sub,sup,removeformat,cleanup,charmap,emotions,|,formatselect,fontselect,fontsizeselect,image",
        extended_valid_elements: "pre[name|class]"
    });  
</script>


<div class="grid_1">&nbsp;</div>

<div class="grid_9">
<div class="formulario">
<?php echo $this->Form->create('Post');?>
	<fieldset>
		<legend><?php echo __('Add Post'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('slug');
		echo $this->Form->input('excerpt');
		echo $this->Form->input('description');
		echo $this->Form->input('pbody', array('rows' => '30'));
		echo $this->Form->input('rss');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>

<div class="grid_1">&nbsp;</div>
