<!DOCTYPE HTML>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title> 
		<?php echo"Laut Pro: ".$title_for_layout; ?>
	</title>
	<meta name="Author" content="guzman6001" />
	<meta name="Subject" content="Red Profesional en Español" />
	<meta name="Keywords" content="Red Social, Trabajo, Metodología, Herramientas" />
	<meta name="Generator" content="Notepad++" />
	<meta name="Language" content="Spanish" />
	<meta name="apple-mobile-web-app-capable" content="yes" /> 
	<meta name="apple-mobile-web-app-status-bar-style" content="grey" /> 
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" /> 

	<?php 
		echo $this->Html->meta('icon');
		
		// JAVASCRIPT:
		//echo $this->Html->script('rdv1/jquery');
		echo $this->Html->script('rdv1/custom');
		//echo $this->Html->script('rdv1/google.map');
		echo $this->Html->script('rdv1/jquery.isotope.min');
		echo $this->Html->script('rdv1/jquery.nivo.slider');
		echo $this->Html->script('rdv1/jquery.pretyPhoto');
		echo $this->Html->script('tipsy');
		echo $this->Html->script('tiny_mce/tiny_mce');
		
		
		
		// CSS:
		
		echo $this->Html->css('rdv1/fonts');
		echo $this->Html->css('rdv1/media');
		echo $this->Html->css('rdv1/prettyPhoto');
		echo $this->Html->css('rdv1/transitions');
		echo $this->Html->css('rdv1/web-style');
		echo $this->Html->css('tipsy');
		//echo $this->Html->css('jquery.masonry.min');
		//echo $this->Html->css('guzman6001');
		echo $scripts_for_layout;
	?>
	<script>
	//// Start Simple Sliders ////
	$(function() {
		setInterval("rotateDiv()", 10000);
	});
		
		function rotateDiv() {
		var currentDiv=$("#simpleslider div.current");
		var nextDiv= currentDiv.next ();
		if (nextDiv.length ==0)
			nextDiv=$("#simpleslider div:first");
		
		currentDiv.removeClass('current').addClass('previous').fadeOut('2000');
		nextDiv.fadeIn('3000').addClass('current',function() {
			currentDiv.fadeOut('2000', function () {currentDiv.removeClass('previous');});
		});
	
	}
	//// End Simple Sliders //// 
</script> 
<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
		$("a[rel^='prettyPhoto']").prettyPhoto();
	});
  
	$(function() 
	{
		$('.img-menu').tipsy({gravity: 'n',title: 'alt'});
		$('.menu').tipsy({gravity: $.fn.tipsy.autoNS});
		$('.mini-img').tipsy({gravity: 's',title: 'alt'});
		$('.img-sn').tipsy({gravity: 's',title: 'alt'});
		$('a').tipsy({gravity: $.fn.tipsy.autoWE});
	});

</script>
</head>

<body>
<div id="header">
    	<!-- Start navigation area -->
        <div id="navigation">

        	<div id="navigation_wrap">

                <!-- Start contact info area -->
                <div id="conteactinfo"><strong>Laut Pro</strong> ~ <?php echo $title_for_layout; ?></div>
                <!-- End contact info area -->
                <!-- Start navigation -->
					<?php echo $this->element('rdv1/menu'); ?>
                <!-- End navigation -->
			</div>
        </div>
        <!-- End navigation area -->
        <div class="clear"></div>
        <!-- Start Social & Logo area -->
        <div id="header_small">
        	<!-- Start Social area -->
        	<div class="social">
            	
                <?php echo $this->element('rdv1/tools'); ?>
                
            </div>
            <!-- End Socialarea -->
            
            <!-- Start Logo area -->
            <div id="logo">
              <a href="/" title="Inicio">Laut.com.ve</a>
            </div>
            <!-- End Logo area -->
        </div>
        <!-- End Social & Logo area -->
</div>

<?php echo $content_for_layout; ?>

<!-- Start Footer -->
<div id="footer">
    <div class="clear"></div>
    <!-- Start Footer Bottom -->
    <div id="footerbottom">
    
    	<div class="footerwrap">
        
        	<!-- Start Copyright Div -->
            <div id="copyright">&copy; 10/12/2013 Laut by 
			<a href="http://www.guzman6001.com.ve" title="Héctor Guzmán">guzman6001</a> & 
            
            <!-- PLEASE SUPPORT US BY LEAVING THIS LINK -->
            <a href="http://www.derby-web-design-agency.co.uk" title="Theme Provided By UBL Designs">UBL Designs</a></div>
            <!-- PLEASE SUPPORT US BY LEAVING THIS LINK -->
            
            <!-- End Copyright Div -->

            <!-- Start Social area -->
            <div class="socialfooter">
                
                <?php echo $this->element('rdv1/tools'); ?>
                
            </div>
            <!-- End Socialarea -->
        
        </div>
    
    </div>
    <!-- End Footer Bottom -->
</div>
<!-- End Footer -->
<!-- Start Scroll To Top Div -->
<div id="scrolltab"></div>
<!-- End Scroll To Top Div -->


<!-- AQUI SE SEPARAN -->
	<!-- BANNER -->	
	<!-- MENSAJE FLASH -->
	<div id="message">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->element('iedetector'); ?>
	</div><div class="clear"></div>
</body>
</html>
