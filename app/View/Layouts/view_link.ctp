<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title><?php echo"Laut Pro - ".$title_for_layout; ?></title>
<meta name="description" content="Laut Pro es una herramienta que te ayudar� a organizar tu tiempo y esfuerzo eficientemente, para que logres realizar tus proyectos de una manera sencilla">
<meta name="keywords" content="">

<!-- Mobile viewport -->
<meta name="viewport" content="width=device-width; initial-scale=1.0">

<link rel="shortcut icon" href="/files/rdv2/images/favicon.ico"  type="image/x-icon" />

<!-- CSS-->
<!-- Google web fonts. You can get your own bundle at http://www.google.com/fonts. Don't forget to update the CSS accordingly!-->
<link href='http://fonts.googleapis.com/css?family=Droid+Serif|Ubuntu' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="/files/rdv2/css/normalize.css">
<link rel="stylesheet" href="/files/rdv2/js/flexslider/flexslider.css" />
<link rel="stylesheet" href="/files/rdv2/css/basic-style.css">
<link rel="stylesheet" href="/css/guzman6001.css">
<!-- end CSS-->
    
<!-- JS-->
<script src="/files/rdv2/js/libs/modernizr-2.6.2.min.js"></script>
<!-- prefix free to deal with vendor prefixes -->
<script src="http://thecodeplayer.com/uploads/js/prefixfree-1.0.7.js" type="text/javascript" type="text/javascript"></script>
<!-- jQuery -->
<script src="http://thecodeplayer.com/uploads/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script>
/*jQuery time*/
$(document).ready(function(){
	$("#accordian h3").click(function(){
		//slide up all the link lists
		$("#accordian ul ul").slideUp();
		//slide down the link list below the h3 clicked - only if its closed
		if(!$(this).next().is(":visible"))
		{
			$(this).next().slideDown();
		}
	});
});
</script>
<!-- end JS-->
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
</head>

<body id="home" style="margin:0px;padding:0px;overflow:hidden">
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

  
<!-- header area -->
    <header class="wrapper clearfix volver">
		       
        <div id="banner">        
        	<div id="logo"><a href="/">Laut</a></div> 
        </div>
		<?php 

$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<nav id="topnav" role="navigation">
	<div class="menu-toggle">OPCIONES</div>  
		
	<ul class="srt-menu" id="menu-main-navigation">
	<li><a target="_blank" href="<?php echo $link['Enlace']['url'] ?>">Quitar Barra</a></li>
<?php if($isok) { ?>
		<li><a href="/enlaces/mostrar/<?php echo $link['Enlace']['id'] ?>">Regresar</a></li>
		<li><a href="/p/inicio">Herramientas</a> 
			<ul>
				<li><a href="/enlaces">Enlaces</a></li>
				<li><a href="/notas">Notas</a></li>
			</ul>
		</li>
		<li><a href="/usuario/perfil/<?= AuthComponent::user('username') ?>"><?= AuthComponent::user('name') ?> <?= AuthComponent::user('lastname') ?></a>
			<ul>
				<li><a href="/usuario/editar_perfil">Editar Perfil</a></li>
				<li><a href="/usuario/perfil/<?= AuthComponent::user('username') ?>">Ver Perfil</a></li>
				<!--li><a href="/mensajes">Mensajes</a></li-->
			</ul>
		</li>
		<li><a href="/usuario/salir">Salir</a></li>
<?php }else{ ?>
		<li><a href="/usuario">Usuario</a>
			<ul>
				<li><a href="/usuario/acceder">Acceder</a></li>
				<li><a href="/usuario/registro">Registro</a></li>
			</ul>
		</li>
<?php } ?>		
	</ul>
</nav><!-- end main navigation -->
  
    </header><!-- end header -->
 


<div id="message"><?php echo $this->Session->flash(); ?></div>




<?php echo $content_for_layout; ?>




<!-- footer area -->  
<!-- GOOGLE ANALITYCS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48515569-1', 'laut.com.ve');
  ga('send', 'pageview');
  
</script>
<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/files/rdv2/js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<script defer src="/files/rdv2/js/flexslider/jquery.flexslider-min.js"></script>

<!-- fire ups - read this file!  -->   
<script src="/files/rdv2/js/main.js"></script>
<!-- GOOGLE ANALITYCS -->
<script type="text/javascript">stLight.options({publisher: "2235a49b-b069-45c0-a292-e094270cdd24", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script>
var options={ "publisher": "2235a49b-b069-45c0-a292-e094270cdd24", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "linkedin", "", "googleplus", "email", "sharethis"]}};
var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
</script>
</body>
</html>