<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title><?php echo"Laut - ".$title_for_layout; ?></title>

<!-- Mobile viewport -->
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
<link rel="shortcut icon" href="/files/rdv2/images/favicon.ico"  type="image/x-icon" />
<style>
.carteluo
{
	<?php 
	srand ();  //Introducimos la "semilla"
	$f = rand(1,2);
	$l = rand(1,2);
?>
	background: url(/img/lautlogo_100.png) no-repeat, url(/img/letras/letras<?php echo $l; ?>.png) no-repeat, url(/img/fondos/fondo<?php echo $f; ?>.png) repeat-x;
	
}
</style>
<!-- CSS-->
<link rel="stylesheet" href="/files/rdv2/css/normalize.css">
<link rel="stylesheet" href="/files/rdv2/js/flexslider/flexslider.css" />
<link rel="stylesheet" href="/files/rdv2/css/basic-style.css">
<link rel="stylesheet" href="/css/guzman6001.css">
<!-- end CSS-->
    
<!-- JS-->
<script src="/files/rdv2/js/libs/modernizr-2.6.2.min.js"></script>
<!-- prefix free to deal with vendor prefixes -->
<script src="http://thecodeplayer.com/uploads/js/prefixfree-1.0.7.js" type="text/javascript" type="text/javascript"></script>
<!-- jQuery -->
<script src="http://thecodeplayer.com/uploads/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script>
/*jQuery time*/
$(document).ready(function(){
	$("#accordian h3").click(function(){
		//slide up all the link lists
		$("#accordian ul ul").slideUp();
		//slide down the link list below the h3 clicked - only if its closed
		if(!$(this).next().is(":visible"))
		{
			$(this).next().slideDown();
		}
	});
});
function loaded()
{
	document.getElementById('tope').style.visibility = 'hidden';
	$('.formulario').on( 'keyup', 'textarea', function (e){
		$(this).css('height', 'auto' );
		$(this).height( this.scrollHeight );
	});
	$('#formulario').find( 'textarea' ).keyup();
} 
</script>
<!-- end JS-->
</head>

<body id="home" onload="loaded()">
<!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

  
<!-- header area -->
    <header class="wrapper clearfix">
		       <div class="nuevologo"><a href="/inicio"><img style="max-width: 300px; height:45px !important;"  src="/img/lautlogo.png" /></a><a href="/inicio">Laut</a>
			   </div>
    
        
        <!-- main navigation -->
		<?php echo $this->element('rdv2/menu'); ?>
        
  
    </header><!-- end header -->
 


<div id="message"><?php echo $this->Session->flash(); ?></div>
<div class="contenido clearfix"><?php echo $content_for_layout; ?>
</div>

<!-- footer area -->    
<footer>
	<div id="colophon" class="wrapper clearfix">
    	<a href="/">Laut.com.ve</a>
    </div>
    
<!--You can NOT remove this attribution statement from any page, unless you get the permission from prowebdesign.ro--><div id="attribution" class="wrapper clearfix" style="color:#666; font-size:11px;">&copy; 10/12/2014 por <a href="/usuario/perfil/guzman6001" style="color:#777;">Héctor Guzmán</a>. Plantilla base: <a href="http://www.prowebdesign.ro/simple-responsive-template/" target="_blank" title="Simple Responsive Template is a free software by www.prowebdesign.ro" style="color:#777;">Simple Responsive Template</a>. <!-- http://contadores.miarroba.es  >
<script type="text/javascript" src="http://contadores.miarroba.es/ver.php?id=674856"></script>
 http://contadores.miarroba.es  -->

</div><!--end attribution-->  
</footer><!-- #end footer area --> 
  

<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/files/rdv2/js/libs/jquery-1.9.0.min.js">\x3C/script>')</script>

<script defer src="/files/rdv2/js/flexslider/jquery.flexslider-min.js"></script>

<!-- fire ups - read this file!  -->   
<script src="/files/rdv2/js/main.js"></script>
<!-- GOOGLE ANALITYCS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48515569-1', 'laut.com.ve');
  ga('send', 'pageview');
  
</script>
<!-- GOOGLE ANALITYCS -->
</body>
</html>