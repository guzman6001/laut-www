<?php $this->Html->addCrumb('Inicio', '/sitio'); ?>
<?php $this->Html->addCrumb('Quote', '/quotes'); ?>

<div class="grid_3">&nbsp;</div>

<div class="grid_5">
<div class="formulario">
<?php echo $this->Form->create('Quote');?>
	<fieldset>
		<legend><?php echo __('Edit Quote'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('qbody');
		echo $this->Form->input('author');
		echo $this->Form->input('rss');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>

<div class="grid_3">&nbsp;</div>
