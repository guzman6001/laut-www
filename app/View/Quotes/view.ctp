<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Frases', '/quotes'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_11"><center>
<h2><?php echo h($quote['Quote']['title']); ?></h2><hr />
<table class="tablita completa">
<tr><td><center>
<span class="quote" style="font-size:350%;">
"<?php echo h($quote['Quote']['qbody']); ?>"
</span>
</center>
<p align="right">- <?php echo h($quote['Quote']['author']); ?></p>
</td></tr>
<tr><td>
<?php echo $this->element('social'); ?>
</td></tr>
<tr>
	<td class="actions"><center>				
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit Quote'), array('action' => 'edit', $quote['Quote']['id'])); ?>
        	<?php echo $this->Form->postLink(__('Delete Quote'), array('action' => 'delete', $quote['Quote']['id']), null, __('Are you sure you want to delete # %s?', $quote['Quote']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
</table>
</center></div>
