<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Frases"),
    	'link' => $this->Html->link(__('Frases'), array('controller' => 'quotes', 'action' => 'index.rss')),
    	'description' => __("Frases recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($quotes as $post) 
{
	if ($post['Quote']['rss'])
	{
    		$postTime = strtotime($post['Quote']['created']);
		$bodyText='"'.$post['Quote']['qbody'].'" ('.$post['Quote']['author'].')';
    		$postLink = '/quotes/view/'.$post['Quote']['id'];

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Quote']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'pubDate' => $post['Quote']['created']
    		));
	}
}
