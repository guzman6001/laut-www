<?php $this->Html->addCrumb('Inicio', '/sitio'); ?>
<?php $this->Html->addCrumb('Videos', '/videos'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>
<div class="grid_9"><center>
<h2><?php echo __('Videos');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'videos','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr />

<?php if (empty($videos))
{ echo ('No se encontraron videos.'); } else {
foreach ($videos as $video): ?>

<table class="tablita izquierda" cellpadding="0" cellspacing="0">

<tr><th colspan="2">
<?php echo $video['Video']['title']; ?>
</th></td> 

<tr>
	<td><center><a title="<?php echo($video['Video']['title']); ?>" href="/videos/view/<?php echo($video['Video']['id']); ?>/<?php echo($video['Video']['slug']); ?>">
		<img src="http://img.youtube.com/vi/<?php echo($video['Video']['code']); ?>/1.jpg" />
		</a></center>
	</td>
	<td rowspan="2"  height='60px' style="vertical-align: middle;<?php 
	if ($video['Video']['favorite'])
	{ 
		echo ' background-color:#f8f9cc;';
	} ?>">
	 
	<?php echo($video['Video']['description']); ?>


	<?php if ($isok){ ?><br /><center>			
	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $video['Video']['id'])); ?>
	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $video['Video']['id']), null, __('Are you sure you want to delete # %s?', $video['Video']['id']));echo "</center>";}?>
	</td>
</tr>
<tr><td class="actions"><center>
	<?php echo $this->Html->link('Ver video', 
		array('action' => 'view', $video['Video']['id'], $video['Video']['slug']),
		array('escape' => false,'title'=>$video['Video']['description'])); ?>
</center></td></tr>
</table>

<?php endforeach; 
}?>
<?php echo $this->element('paginator'); ?>
</center></div>

<div class="grid_2">
	<?php echo $this->element('publicidad_vertical'); ?>
</div>
