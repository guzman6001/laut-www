<?php $this->Html->addCrumb('Inicio', '/sitio'); ?>
<?php $this->Html->addCrumb('Videos', '/videos'); ?>

<div class="grid_3">&nbsp;</div>

<div class="grid_5">
<div class="formulario">
<?php echo $this->Form->create('Video');?>
	<fieldset>
		<legend><?php echo __('Add Video'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('slug');
		echo $this->Form->input('code');
		echo $this->Form->input('favorite');
		echo $this->Form->input('rss');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>


<div class="grid_3">&nbsp;</div>
