<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Videos"),
    	'link' => $this->Html->link(__('Videos'), array('controller' => 'videos', 'action' => 'index.rss')),
    	'description' => __("Videos recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($videos as $post) 
{
	if ($post['Video']['rss'])
	{
    		$postTime = strtotime($post['Video']['created']);
		$bodyText=$post['Video']['description'];
    		$postLink = '/videos/view/'.$post['Video']['id'].'/'.$post['Video']['slug'];

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Video']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Video']['created']
    		));
	}
}
