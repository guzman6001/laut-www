<?php $this->Html->addCrumb('Inicio','/sitio'); ?>

<div class="grid_11">
<center>

<!-- REDES SOCIALES -->
<table class="tablita">
<tr><th>Redes Sociales, Páginas y Perfiles Públicos</th></tr>
<tr><td><center>
<?php echo $this->Html->link(
	$this->Html->image('icons/facebook.png', 
	array('alt'=> __('Facebook', true), 'border' => '0','class'=>'img-sn')),
	'https://www.facebook.com/guzman6001page', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/twitter.png', 
	array('alt'=> __('Twitter', true), 'border' => '0','class'=>'img-sn')),
	'http://twitter.com/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/identica.png', 
	array('alt'=> __('Identi.ca', true), 'border' => '0','class'=>'img-sn')),
	'http://identi.ca/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/linkedin.png', 
	array('alt'=> __('LinkedIn', true), 'border' => '0','class'=>'img-sn')),
	'http://ve.linkedin.com/pub/héctor-guzmán/32/341/555', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/tumblr.png', 
	array('alt'=> __('Tumblr', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.tumblr.com', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;<br />
<?php echo $this->Html->link(
	$this->Html->image('icons/wpblog.png', 
	array('alt'=> __('Blog en Wordpress', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.wordpress.com/', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/stripgenerator.png', 
	array('alt'=> __('Stripgenerator', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.stripgenerator.com/gallery', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/pixton.png', 
	array('alt'=> __('Pixton', true), 'border' => '0','class'=>'img-sn')),
	'http://www.pixton.com/es/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/deviant.png', 
	array('alt'=> __('Deviant', true), 'border' => '0','class'=>'img-sn')),
	'http://guzman6001.deviantart.com/', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/subcultura.png', 
	array('alt'=> __('Subcultura', true), 'border' => '0','class'=>'img-sn')),
	'http://subcultura.es/user/guzman6001/', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/goear.png',  
	array('alt'=> __('Goear', true), 'border' => '0','class'=>'img-sn')),
	'http://www.goear.com/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/jamendo.png',  
	array('alt'=> __('Jamendo', true), 'border' => '0','class'=>'img-sn')),
	'http://www.jamendo.com/es/user/guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/dropbox.png',  
	array('alt'=> __('Dropbox', true), 'border' => '0','class'=>'img-sn')),
	'http://db.tt/gkipuKv', 
	array('escape' => false,'target'=>'_blank'));
?>&nbsp;
<?php echo $this->Html->link(
	$this->Html->image('icons/imageshack.png', 
	array('alt'=> __('Imageshack', true), 'border' => '0','class'=>'img-sn')),
	'http://imageshack.us/homepage/?user=guzman6001', 
	array('escape' => false,'target'=>'_blank'));
?>
</center></td></tr></table></center>
</div>


<div class="grid_11">
<center>




<!-- BLOG -->
<table class="tablita izquierdo" style="clear: left;">
<tr><th><?php echo __('Lastest Posts'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'posts','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td>
<?php if(empty($posts)): ?>
No hay posts guardados por el momento.
<?php else:
foreach ($posts as $post): ?>
	<ul>
	<li>
	<a	title="<?php echo $post['Post']['description'] ?>"
		href="posts/view/<?php echo $post['Post']['id'] ?>/<?php echo $post['Post']['slug'] ?>">
			<?php echo $post['Post']['title'] ?>
	</a>
	</li>
	</ul>
<?php endforeach; ?><br />
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'posts','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>






<!-- COMICS -->
<table class="tablita izquierdo">
<tr><th><?php echo __('Dibujo/Tira más reciente'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'comics','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td>
<?php if(empty($comics)): ?>
Inauguración este viernes 9 de Marzo.
<?php else:
foreach ($comics as $comic): ?>
<center><img 
	alt="<?php echo $comic['Comic']['title']; ?>"
	title="<?php echo $comic['Comic']['title']; ?>"
	style="max-width:380px;"
	src="<?php echo ($comic['Comic']['file']); ?>" />
</center>
<?php endforeach; ?><br />
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'comics','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>


<!-- NANOBLOG -->
<table class="tablita izquierdo" style="float:left; clear: left;">
<tr><th><?php echo __('guzman6001 dice...'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'microposts','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td>
<?php if(empty($microposts)): ?>
No hay microposts guardados por el momento.
<?php else:
foreach ($microposts as $micropost): ?>
	<ul>
	<li>
		<?php echo $micropost['Micropost']['mpbody'] ?>
	</li>
	</ul>
<?php endforeach; ?><br />
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'microposts','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>


<!-- VIDEOS -->
<table class="tablita izquierdo">
<tr><th><?php echo __('Lastest Video'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'videos','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td>
<?php if(empty($videos)): ?>
No hay videos guardados por el momento.
<?php else:

foreach ($videos as $video): ?>
	<div>
		<iframe width="100%" height="315" 
		src="http://www.youtube.com/embed/<?php echo $video['Video']['code']; ?>?rel=0" 
		frameborder="0"></iframe>

	</div>
<?php endforeach; ?><br />
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'videos','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>





<!-- ENLACES -->
<table class="tablita izquierdo" style="clear: left;">
<tr><th><?php echo __('Lastest Links'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'links','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td>
<?php if(empty($links)): ?>
No hay links guardados por el momento.
<?php else:
foreach ($links as $link): ?>
	<ul>
	<li>
	<a	title="<?php echo $link['Link']['description'] ?>"
		target='_blank'
		href="<?php echo $link['Link']['url'] ?>">
			<?php echo $link['Link']['title'] ?>
	</a>
	</li>
	</ul>
<?php endforeach; ?><br />
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'links','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>


<!-- IMAGENES -->
<table class="tablita izquierdo">
<tr><th><?php echo __('Lastest Image'); ?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'images','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></th></tr>
<tr><td>
<?php if(empty($images)): ?>
No hay videos guardados por el momento.
<?php else:
foreach ($images as $image): ?>
<center><img 
	alt="<?php echo $image['Image']['title']; ?>"
	title="<?php echo $image['Image']['title']; ?>"
	style="max-width:380px;"	
	src="<?php echo ($image['Image']['file']); ?>" /></center>

<?php endforeach; ?>
<center><?php echo $this->Html->link(__('View more >>'), 
	array('controller' => 'images','action' => 'index'));?></center>
<?php endif; ?>
</td></tr></table>






</center>
</div>



