<?php $this->Html->addCrumb('Administrar', '/sitio/administracion'); ?>
<?php $this->Html->addCrumb('Categories', '/categories'); ?>

<div class="grid_3">&nbsp;</div>

<div class="grid_5">
<div class="formulario">
<?php echo $this->Form->create('Category');?>
	<fieldset>
		<legend><?php echo __('Add Category'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>

<div class="grid_3">&nbsp;</div>



