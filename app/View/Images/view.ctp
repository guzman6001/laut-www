<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Imágenes', '/images'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_11"><center>
<h2><?php echo ($image['Image']['title']) ;?></h2><hr />
<table class="tablita completa">
<tr><td><center><img 
	alt="<?php echo $image['Image']['title']; ?>"
	title="<?php echo $image['Image']['title']; ?>"
	style="max-width: 680px;"	
	src="<?php echo ($image['Image']['file']); ?>" /></center>
</td>
</tr>
<tr><td><center><?php echo ($image['Image']['description']); ?></center></td></tr>
<tr>
	<td class="actions"><center>
		<a target="_blank" 
			href="<?php echo $image['Image']['source']; ?>">Ver Fuente Original</a>		
		<a target="_blank" 
			href="<?php echo $image['Image']['file']; ?>">Tamaño Completo</a> 
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $image['Image']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $image['Image']['id']), null, __('Are you sure you want to delete # %s?', $image['Image']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
<tr><td>
	<?php echo $this->element('social'); ?>
</td></tr>
</table>
</center></div>
