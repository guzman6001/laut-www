<?php $this->Html->addCrumb('Inicio', '/sitio'); ?>
<?php $this->Html->addCrumb('Imágenes', '/images'); ?>

<div class="grid_3">&nbsp;</div>

<div class="grid_5">
<div class="formulario">
<?php echo $this->Form->create('Image');?>
	<fieldset>
		<legend><?php echo __('Add Image'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('slug');
		echo $this->Form->input('description');
		echo $this->Form->input('file');
		echo $this->Form->input('source');
		echo $this->Form->input('rss');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>

<div class="grid_3">&nbsp;</div>
