<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Imágenes"),
    	'link' => $this->Html->link(__('Images'), array('controller' => 'images', 'action' => 'index.rss')),
    	'description' => __("Imágenes recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($images as $post) 
{
	if ($post['Image']['rss'])
	{
    		$postTime = strtotime($post['Image']['created']);
		$bodyText=$post['Image']['description'];
    		$postLink = '/images/view/'.$post['Image']['id']."/".$post['Image']['slug'];

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Image']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Image']['created']
    		));
	}
}
