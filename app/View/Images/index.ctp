<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Imágenes', '/images');
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_9">
	<center><h2><?php echo __('Images');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'images','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr /></center>
<?php
	foreach ($images as $image):
	/*$style = null;
	if ($i++ % 2 == 0) 
	{
		$style = 'style="clear: left;"';
	}*/ ?>

<center><table class="tablita">
<tr>
	<th><?php echo ($image['Image']['title']); ?></th>
</tr>
<tr><td><center><img 
	alt="<?php echo $image['Image']['title']; ?>"
	title="<?php echo $image['Image']['title']; ?>"
	style="max-width: 680px;"	
	src="<?php echo ($image['Image']['file']); ?>" /></center>
</td>
</tr>
<tr>
	<td class="actions"><center>
	<?php echo $this->Html->link(__('Comentar / Compartir / Ver detalles'), array('action' => 'view', $image['Image']['id'],$image['Image']['slug']));?>
				
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $image['Image']['id'])); ?>
        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $image['Image']['id']), null, __('Are you sure you want to delete # %s?', $image['Image']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
</table></center>
<?php endforeach; ?>
<center><?php echo $this->element('paginator'); ?></center>
</div>

<div class="grid_2">
	<?php echo $this->element('publicidad_vertical'); ?>
</div>