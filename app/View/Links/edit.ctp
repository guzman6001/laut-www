<?php $this->Html->addCrumb('Administrar', '/sitio/administracion'); ?>
<?php $this->Html->addCrumb('Links', '/links/show'); ?>

<div class="grid_2">&nbsp;</div>
<div class="grid_7">
<div class="formulario">
<?php echo $this->Form->create('Link');?>
	<fieldset>
		<legend><?php echo __('Edit Link'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('url');
		echo $this->Form->input('category_id');
		echo $this->Form->input('rss');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>
<div class="grid_2">&nbsp;</div>
