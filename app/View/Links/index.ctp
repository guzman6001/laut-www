<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Enlaces', '/links');
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>
﻿
<div class="grid_11">
<center><h2><?php echo __('Links');?><?php echo $this->Html->link(
	$this->Html->image('icons/mini-rss.png', 
	array('alt'=> __('Suscribe', true), 'border' => '0','align'=>'right','class'=>'mini-img')),
	array ('controller'=>'links','action'=>'index.rss'),	 
	array('escape' => false,'target'=>'_blank'));
?></h2><hr /></center>

<?php if(empty($categories)): ?>
No hay links guardados por el momento.
<?php else:

	foreach ($categories as $category): ?>
<table class="tablita izquierdo">


<tr><th><?php echo ($category['Category']['name']); ?></th></tr>
<tr><td>
	<?php if (!empty($category['Link']))
	{ ?>
	<?php
	foreach ($category['Link'] as $link): ?>
	<ul>
	<li><strong>
	<a	title="<?php echo $link['description'] ?>"
		target='_blank'
		href="<?php echo $link['url'] ?>">
			<?php echo $link['title'] ?>
	</a>
	</strong>
        <?php if ($isok){ ?>(        
        <?php echo $this->Html->link('φ', 
		array('action' => 'edit', $link['id']),
		array('title' => 'Editar')); ?>
        <?php echo $this->Form->postLink('ϰ', 
		array('action' => 'delete', $link['id']), 
		array('title' => 'Eliminar'), 
		__('Are you sure you want to delete # %s?', $link['id']));?>
	)<?php }?>
	</li>
	</ul>			
	<?php endforeach; ?>
<?php 
	}
	else
	{
		echo ('No se encontraron links en esta categoría');
	} ?>
</td></tr></table>
	<?php endforeach; ?>
<?php endif; ?>

<center>
<?php echo $this->element('paginator'); ?>
		<table class="tablita completa">
		<tr><td>
		<script type="text/javascript"><!--
		google_ad_client = "ca-pub-3028399397570881";
		/* blog - guzman6001 */
		google_ad_slot = "4993405013";
		google_ad_width = 728;
		google_ad_height = 90;
		//-->
		</script>
		<script type="text/javascript"
		src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
		</td></tr></table>
</center>
</div>
