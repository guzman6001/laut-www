<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Enlaces"),
    	'link' => $this->Html->link(__('List Links'), array('controller' => 'mostrar', 'action' => 'index.rss')),
    	'description' => __("Enlaces recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($links as $post) 
{
	if ($post['Link']['rss'])
	{
    		$postTime = strtotime($post['Link']['created']);
		$bodyText='<a target="_blank" href="'.
		$post['Link']['url'].'">'.$post['Link']['description'].'</a>';

    		$postLink = $post['Link']['url'];
	/*
    	$bodyText = preg_replace('=\(.*?\)=is', '', $post['Link']['title']);
    	$bodyText = $this->Text->stripLinks($bodyText);
    	$bodyText = Sanitize::stripAll($bodyText);
    	$bodyText = $this->Text->truncate($bodyText, 140, array(
        	'ending' => '...',
        	'exact'  => true,
        	'html'   => true,
    	));*/
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Link']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Link']['created']
    		));
	}
}
