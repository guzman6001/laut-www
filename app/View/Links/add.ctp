<?php $this->Html->addCrumb('Administrar', '/sitio/administracion'); ?>
<?php $this->Html->addCrumb('Links', '/links/show'); ?>

<script language="javascript">
$(document).ready(function() 
{
   // Esta primera parte crea un loader no es necesaria
	$().ajaxStart(function() 
	{
		$('#CategoryAddForm').hide();
		$('#result').hide();
		$('#cargando').fadeIn('slow');
	}).ajaxStop(function() 
	{
		$('#CategoryAddForm').hide();
		$('#result').fadeIn('slow');
	});

   // Interceptamos el evento submit
    	$('#form, #fat, #CategoryAddForm').submit(function() 
	{
  // Enviamos el formulario usando AJAX
		$.ajax(
		{
		    	type: 'POST',
		    	url: $(this).attr('action'),
		    	data: $(this).serialize(),
		    	// Mostramos un mensaje con la respuesta de PHP
			success: function(data) 
			{
			      	$('#CategoryAddForm').hide();
				$('#result').fadeIn('slow');
			}
		})        
        return false;
    	}); 
})  
</script>

<div class="grid_1">&nbsp;</div>
<div class="grid_6">
<div class="formulario">
<?php echo $this->Form->create('Link');?>
	<fieldset>
		<legend><?php echo __('Add Link'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('url');
		echo $this->Form->input('category_id');
		echo $this->Form->input('rss');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>


<div class="grid_4">
	<div class="formulario">
	<?php echo $this->Form->create('Category', array('action' => 'add')); ?>
	<fieldset>
		<legend><?php echo __('Add Category'); ?></legend>
		<?php echo $this->Form->input('name'); ?>
		<?php echo $this->Form->input('description'); ?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit'));?>
	</div>
	<div id="loading" style="display:none;"><h2>Cargando...</h2></div>
	<div id="result" style="display:none;"><h2>Categoría agregada.</h2></div>
</div>
