<?php $this->Html->addCrumb('Inicio', '/'); ?>
<?php $this->Html->addCrumb('Nanoblog', '/micropost'); 
$isok=false;
if ($this->Session->check('Auth.User')) 
{
	$issesions = $this->Session->read('Auth.User');
	if (isset($issesions)){$isok=true;}
}
?>

<div class="grid_11"><center>
<h2><?php echo h($micropost['Micropost']['title']); ?></h2><hr />
<table class="tablita completa">
<tr><td><center>
<span style="font-size:150%;"><i>
<?php echo h($micropost['Micropost']['mpbody']); ?>
</i></span>
</center></td></tr>
<tr>
	<td class="actions"><center>				
		<?php if ($isok){ ?>       
        	<?php echo $this->Html->link(__('Edit Micropost'), array('action' => 'edit', $micropost['Micropost']['id'])); ?>
        	<?php echo $this->Form->postLink(__('Delete Micropost'), array('action' => 'delete', $micropost['Micropost']['id']), null, __('Are you sure you want to delete # %s?', $micropost['Micropost']['id'])); ?>
	<?php }?>
	</center></td>
</tr>
<tr><td>
 <?php echo $this->element('social'); ?>
</td></tr>
</table>
</center></div>
