<?php $this->Html->addCrumb('Inicio', '/sitio'); ?>
<?php $this->Html->addCrumb('Quote', '/quotes'); ?>

<div class="grid_3">&nbsp;</div>

<div class="grid_5">
<div class="formulario">
<?php echo $this->Form->create('Micropost');?>
	<fieldset>
		<legend><?php echo __('Add Micropost'); ?></legend>
	<?php
		echo $this->Form->input('title');
		echo $this->Form->input('mpbody');
		echo $this->Form->input('rss');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
</div>

<div class="grid_3">&nbsp;</div>
