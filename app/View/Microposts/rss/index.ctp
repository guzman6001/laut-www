<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("Héctor Guzmán - Nanoblog"),
    	'link' => $this->Html->link(__('Micropost'), array('controller' => 'microposts', 'action' => 'index.rss')),
    	'description' => __("Micropost recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($microposts as $post) 
{
	if ($post['Micropost']['rss'])
	{
    		$postTime = strtotime($post['Micropost']['created']);
		$bodyText=$post['Micropost']['mpbody'];
    		$postLink = '/microposts';

    	$bodyText = Sanitize::stripAll($bodyText);
	
    		echo  $this->Rss->item(array(), array(
			'title' => $post['Micropost']['title'],
			'link' => $postLink,
			'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
			'description' => $bodyText,
			'dc:creator' => 'guzman6001',
			'pubDate' => $post['Micropost']['created']
    		));
	}
}
