﻿<div id="tope"><img src="/files/rdv2/images/cargando.gif" /><br />CARGANDO</div>
<!-- main content area -->   
<div id="main" class="wrapper">
<center><h2>Notas de <a href="/usuario/perfil/<?php echo $usuario_completo['User']['username'] ?>"><?php echo $usuario_completo['User']['name']." ".$usuario_completo['User']['lastname']; ?></a></h2><hr /></center>  
<!-- content area -->    
<!-- header recent notas: class="active" 
		<li class="active">
			<h3><span class="icon-tasks"></span>Tasks</h3>
-->
	<section id="content" class="wide-content">
    	<div class="grid_4">	
			<div id="accordian">
			<ul>
				<?php 
				if(empty($notacategories))
				{echo"<li><h3>No se han guardado categorias todavía</h3></li>";}
				else
				{ 
				
					foreach ($notacategories as $category): 
					if (!empty($category['Nota']))
					{
						foreach ($category['Nota'] as $nota => &$nose):
							if ($nose['visibility_id']!=1)
							{
								unset($category['Nota'][$nota]);
							}
						endforeach;
					}
					if (!empty($category['Nota']))
					{					?>
					<li>
					<h3><?php echo ($category['Category']['name']); ?></h3>
					<ul>
						<?php 
						
						foreach ($category['Nota'] as $nota): 
						
						?>
						<li>
						<a title="<?php echo $nota['title'] ?>"
						href="/notas/ver/<?php echo $nota['id'] ?>/<?php echo ($nota['slug']); ?>">
							<?php echo $nota['title'] ?>
						</a>
						</li>			
						
						<?php endforeach; ?>
						
					</ul>
					</li>
					<?php 
					}endforeach; ?>	
				<?php 
				} ?>
			</ul>
			</div>
        </div>
        
        <div class="grid_8">

		<?php foreach ($notas as $nota): ?>
			<div class="cuadro_blanco">
			<div class="text0">
				<h3><?php echo ($nota['Nota']['title']); ?></h3><hr />
				<?php echo ($nota['Nota']['description']); ?>
			</div>
			</div>
		<?php endforeach; ?>
		<center><?php echo $this->element('paginator'); ?></center>
		</div>
	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->