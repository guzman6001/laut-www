﻿<?php echo $this->Html->script('tiny_mce/tiny_mce'); ?>
<script type="text/javascript"> 
    tinyMCE.init({ 
        theme : "advanced", 
		skin: "o2k7",
        mode : "exact", 
        elements : "NotaDescription",
        plugins: "codesyntax",  
		preformatted : true,
        convert_urls : false, 
		height : 300,
		
		 // Theme options  
        theme_advanced_buttons1: "cut,copy,paste,|,bold,italic,underline,|,bullist,numlist",
		theme_advanced_buttons2: "iespell,link,unlink,removeformat,emotions,|,image,|,justifyleft,justifycenter,justifyright,justifyfull",
		theme_advanced_buttons3 :"codeformat,|,code",
        extended_valid_elements: "pre[name|class]"
    });  
</script>

<div id="tope"><img src="/files/rdv2/images/cargando.gif" /><br />CARGANDO</div>
<!-- main content area -->   
<div id="main" class="wrapper">
    
    
<!-- content area -->    
<!-- header recent notas: class="active" 
		<li class="active">
			<h3><span class="icon-tasks"></span>Tasks</h3>
-->
	<section id="content" class="wide-content">
    	<div class="grid_4">
		
		<!-- AGREGAR CATEGORIA -->
			<div class="formulario">
				<?php echo $this->Form->create('Category', array('url' => '/notas/nueva_categoria'));?>
				<fieldset>
					<legend><?php echo __('Agregar nueva categoría'); ?></legend>
					<?php 
					echo $this->Form->input('name', array('required' => true));
					 echo $this->Form->input('description'); ?>
				</fieldset>
				<?php echo $this->Form->end(__('Guardar'));?>
				</div>
		
			<div id="accordian">
			<ul>
				<?php 
				if(empty($notacategories))
				{echo"<li><h3>No se han guardado categorias todavía</h3></li>";}
				else
				{ 
				
					foreach ($notacategories as $category): 
					if (!empty($category['Nota']))
					{					?>
					<li>
					<h3><?php echo ($category['Category']['name']); ?></h3>
					<ul>
						<?php 
						
						foreach ($category['Nota'] as $nota): 
						
						?>
						<li>
						<a title="<?php echo $nota['title'] ?>"
						href="/notas/ver/<?php echo $nota['id'] ?>/<?php echo ($nota['slug']); ?>">
							<?php echo $nota['title'] ?>
						</a>
						</li>			
						
						<?php endforeach; ?>
						
					</ul>
					</li>
					<?php 
					}endforeach; ?>	
				<?php 
				} ?>
			</ul>
			</div>
        </div>
        
        <div class="grid_8">
			
			<div class="formulario">
			<?php 
				if(!empty($notacategories))
				{ 
			
			echo $this->Form->create('Nota', array('url' => '/notas/nuevo'));?>
				<fieldset>
					<legend><?php echo __('Agregar nueva nota'); ?></legend>
				<?php
					//echo $this->Form->input('title');
					echo $this->Form->input('category_id');
					echo $this->Form->input('title', array('required' => true));
					echo $this->Form->input('description');
					echo $this->Form->input('slug');
					echo $this->Form->input('visibility_id');
				?>
				</fieldset>
			<?php echo $this->Form->end(__('Guardar'));
			}
			?>
		</div>
            
		
		<?php foreach ($notas as $nota): ?>
			<div class="cuadro_blanco">
			<div class="text0">
				<h3><?php echo ($nota['Nota']['title']); ?></h3><hr />
				<?php echo ($nota['Nota']['description']); ?>
				<br /><br /> 
		<center><a class="button_verde" href="/notas/ver/<?php echo h($nota['Nota']['id']); ?>/<?php echo h($nota['Nota']['slug']); ?>">Abrir</a> <a class="button_verde" href="/notas/editar/<?php echo h($nota['Nota']['id']); ?>">editar</a></center>
			</div>
			</div>
		<?php endforeach; ?>
		<center><?php echo $this->element('paginator'); ?></center>
		</div>
	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->