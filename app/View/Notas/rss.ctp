<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("LAUT RSS - NOTAS"),
    	'link' => $this->Html->link(__('Nota'), array('controller' => 'notas', 'action' => 'index.rss')),
    	'description' => __("Notas recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($notas as $post) 
{
		$postTime = strtotime($post['Nota']['created']);
	$bodyText='"'.$post['Nota']['description'].'" ('.$post['User']['name'].')';
		$postLink = '/notas/ver/'.$post['Nota']['id'];

	$bodyText = Sanitize::stripAll($bodyText);

		echo  $this->Rss->item(array(), array(
		'title' => $post['Nota']['title'],
		'link' => $postLink,
		'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
		'description' => $bodyText,
		'dc:creator' => $post['User']['name'],
		'pubDate' => $post['Quote']['created']
		));
}
