﻿<div id="tope"><img src="/files/rdv2/images/cargando.gif" /><br />CARGANDO</div>
<!-- main content area -->   
<section id="hero">    
    <!-- responsive FlexSlider image slideshow -->
    <div class="wrapper clearfix">
        <div class="grid_8">
			<div class="cuadro_blanco clearfix"><div>
            <p align="justify" style="margin: 0px !important;"><a href="/inicio"><img style="float: left; margin: 0px 15px 15px 0px; width:50px" src="/img/lautlogo.png" /></a> 
            Laut es un conjunto de herramientas en línea que te ayudarán a organizar tus recursos y acceder a ellos fácilmente.
            </p>
            </div></div>
			<!-- SEPARADOR -->
			<div class="flexslider">
                    <ul class="slides">
                        <li>
                            <a href="/enlaces"><img src="/files/rdv2/images/enlaces.jpg" /></a>
                            <p class="flex-caption">Guarda y accede a tus enlaces fácilmente.</p>
                        </li>
						
						<li>
                            <a href="/notas"><img src="/files/rdv2/images/notas.jpg" /></a>
                            <p class="flex-caption">Crea tus notas y organízalas en categorías.</p>
                        </li>
                    </ul>
                  </div>
			<br /><br />
			<!-- SEPARADOR -->
			<div class="grid_6_absolute">
        	<center><h1 class="first-header"><i class="fa fa-link"></i> <a href="/enlaces">Enlaces</a></h1></center>
			<div class="cuadro_blanco"><div>
			<?php echo $enlacestexto['Nota']['description']; ?>	
			</div></div>
			
           <div class="cuadro_blanco"><div><h3>Enlaces recientes</h3><hr /><ul><?php
				foreach ($enlaces as $enlace): 
					echo "<li><span style='font-size:small'><a href='/usuario/perfil/".$enlace['User']['username']."'>".$enlace['User']['name']."</a> publicó <a  href='/enlaces/ver/".$enlace['Enlace']['id']."'>".$enlace['Enlace']['title']."</a>.</span></li>";
	
				endforeach; ?>
			</ul></div></div>
        </div>
        
		<div class="grid_6_absolute">
			<center><h1 class="first-header"><i class="fa fa-file-text-o"></i> <a href="/notas">Notas</a></h1></center>
			<div class="cuadro_blanco"><div>
			<?php echo $notastexto['Nota']['description']; ?>	
			</div></div>
			
			<div class="cuadro_blanco"><div><h3>Notas recientes</h3><hr /><ul>
			<?php
				foreach ($notas as $enlace): 
					echo "<li><span style='font-size:small'><a  href='/notas/ver/".$enlace['Nota']['id']."/".$enlace['Nota']['slug']."'>".$enlace['Nota']['title']."</a> de <a href='/usuario/perfil/".$enlace['User']['username']."'>".$enlace['User']['name']."</a>.</span></li>";
					?>
			<?php	
				endforeach;
			?>
			</ul></div></div>
		</div>
			
        </div>
        <div class="grid_4">
		<div class="formulario">
<?php echo $this->Form->create('User', array(
    'url' => array('controller' => 'usuario', 'action' => 'acceder')));?>
<fieldset>	
	<legend><?php echo __('Acceder'); ?></legend>
	<?php
		echo $this->Form->input('username',array(
     'placeholder' => 'Usuario','label' => false));
		echo $this->Form->input('password',array(
     'placeholder' => 'Contraseña','label' => false));
	?>
</fieldset>
<?php echo $this->Form->end(__('Acceder'));?>
</div>
      <br /><br />          
				  
                <!-- FlexSlider -->
				<div><center><h1><i class="fa fa-twitter"></i> <a href="https://twitter.com/lautve" target="_blank">Twitter</a></h1>
            <a class="twitter-timeline" href="https://twitter.com/lautve" data-widget-id="442371645886324736">Tweets por @lautve</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></center></div>
<div class="clear"><center><a href="https://db.tt/gkipuKv" target="_blank"><img src="http://guzman6001.files.wordpress.com/2014/03/dropbox.gif" /></a></center></div>  
        </div>
    </section><!-- end hero area -->