<?php 

$this->set('documentData', array(
    'xmlns:dc' => 'http://purl.org/dc/elements/1.1/'));

$this->set('channelData', 
	array(
	'title' => __("LAUT RSS - NOTAS"),
    	'link' => $this->Html->link(__('Nota'), array('controller' => 'notas', 'action' => 'ver')),
    	'description' => __("Notas recientes"),
    	'language' => 'en-us'));

App::uses('Sanitize', 'Utility');

foreach ($notas as $post) 
{
		$postTime = strtotime($post['Nota']['created']);
	$autor="Alguien";
	if(isset($post['User']['twitter']) && $post['User']['twitter']!="")
	{
		$autor="@".$post['User']['twitter'];
	}
	else
	{
		$autor=$post['User']['name'];
	}
	$bodyText=$autor.' public&oacute;: '.$post['Nota']['title'].' http://laut.com.ve/notas/ver/'.$post['Nota']['id'];
		$postLink = '/notas/ver/'.$post['Nota']['id'];

	$bodyText = Sanitize::stripAll($bodyText);

		echo  $this->Rss->item(array(), array(
		'title' => $post['Nota']['title'],
		'link' => $postLink,
		'guid' => array('url' => $postLink, 'isPermaLink' => 'true'),
		'description' => $bodyText,
		//'dc:creator' => "nota",
		'pubDate' => $post['Nota']['created']
		));
}
