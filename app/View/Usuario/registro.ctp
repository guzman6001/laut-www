<div id="main" class="wrapper">
<div class="grid_2">&nbsp;</div>
<div class="grid_8">

<div class="formulario">
<?php echo $this->Form->create('User');?>
	<fieldset>
		<legend><?php echo __('Registro'); ?></legend>
	<?php
		echo $this->Form->input('username',array('label'=>'Nombre de usuario'));
		echo $this->Form->input('email',array('label'=>'Correo electrónico'));
		echo $this->Form->input('password',array('label'=>'Contraseña','class' => 'input password required'));
		echo $this->Form->input('password_confirm', array(
			'type' => 'password',
			'class' => 'input password required',
			'label' => 'Confirme contraseña'
		));
		
		echo $this->Form->input('name',array('label'=>'Nombre'));
		echo $this->Form->input('lastname',array('label'=>'Apellido'));
		echo $this->Form->input('country_id',array('default'=>'232','label'=>'País'));
		$this->Captcha->render($captchaSettings);
	?>
	</fieldset>
<?php echo $this->Form->end(__('Registro'));?>
</div>


</div>
<div class="grid_2">&nbsp;</div>
</div>