<!-- main content area -->   
<div id="main" class="wrapper">
    
    
<!-- content area -->    
	<section id="content" class="wide-content">
    	<div class="grid_4">
        	<h1 class="first-header">Perfil</h1>
           <strong>Nombre de Usuario</strong>: <?= AuthComponent::user('username') ?><br /><br />
		   <strong>Nombre</strong>: <?= AuthComponent::user('name') ?><br /><br />
		   <strong>Apellido</strong>: <?= AuthComponent::user('lastname') ?><br /><br />
		   <strong>Correo</strong>: <?= AuthComponent::user('email') ?><br /><br />
        </div>
        
        <div class="grid_8">
        	<h1 class="first-header">Publicaciones recientes</h1>
            <?php if (empty($lations))
			{
				echo ('<center>- No has generado contenido aun -</center>'); 
			} 
			else 
			{
				foreach ($lations as $lation): 
			?>	
			
				
				<div class="enlace_seleccionado">
				<?php
					if ($lation['Tipo']['name']=="Enlace")
					{
						echo "<h3><a style='color:#125c00;' target='_blank' href='".$lation['Lation']['url']."'>".$lation['Lation']['title']."</a></h3>";
					}
					else
					{
						"<h3>".$lation['Lation']['title']."</h3>";
					}
				?>
					<?php echo($lation['Lation']['description']); ?>
				</div>
			<?php	
				
				endforeach;
			}?>
        </div>

	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->