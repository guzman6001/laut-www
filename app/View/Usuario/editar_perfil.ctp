<div class="grid_8">
<div class="formulario">
<?php echo $this->Form->create('User');?>
	<fieldset>
		<legend><?php echo __('Datos básicos'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('label'=>'Nombre'));
		echo $this->Form->input('lastname',array('label'=>'Apellido'));
		echo $this->Form->input('foto',array('label'=>'Foto (&)'));
		echo $this->Form->input('biografia',array('label'=>'Biografía'));
	?>
	</fieldset>
	<fieldset>
		<legend><?php echo __('Datos personales'); ?></legend>
	<?php
		echo $this->Form->input('country_id',array('default'=>'232','label'=>'País'));
		echo $this->Form->input('ciudad',array('label'=>'Ciudad'));
		echo $this->Form->input('comidas',array('label'=>'Comidas favoritas'));
		echo $this->Form->input('twitter',array('label'=>'Usuario Twitter (sin @)'));
		echo $this->Form->input('web',array('label'=>'Enlace de tu página o blog'));
		echo $this->Form->input('aficiones',array('label'=>'Aficiones'));
		echo $this->Form->input('equipos_favoritos',array('label'=>'Equipos favoritos'));
		echo $this->Form->input('personal_adicional',array('label'=>'Información adicional'));
	?>
	</fieldset>
	<fieldset>
		<legend><?php echo __('Datos profesionales'); ?></legend>
	<?php
		echo $this->Form->input('linkedin',array('label'=>'Perfil de LinkedIn'));
		echo $this->Form->input('proyectos',array('label'=>'Proyectos'));
		echo $this->Form->input('conocimientos',array('label'=>'Habilidades / conocimientos'));
		echo $this->Form->input('profesional_adicional',array('label'=>'Información adicional'));	
	?>
	</fieldset>
	<fieldset>
		<legend><?php echo __('Datos académicos'); ?></legend>
	<?php
		echo $this->Form->input('titulo',array('label'=>'Título obtenido'));
		echo $this->Form->input('universidad',array('label'=>'Institución'));
		echo $this->Form->input('logo_universidad',array('label'=>'Logo de la institución (&)'));
		echo $this->Form->input('pagina_universidad',array('label'=>'Web de la institución'));
	?>
	</fieldset>
	<fieldset>
		<legend><?php echo __('Datos laborales'); ?></legend>
	<?php	
		echo $this->Form->input('empresa',array('label'=>'Empresa'));
		echo $this->Form->input('logo_empresa',array('label'=>'Logo de la empresa (&)'));
		echo $this->Form->input('pagina_empresa',array('label'=>'Web de la empresa'));
		echo $this->Form->input('cargo',array('label'=>'Cargo actual'));
	?>
	</fieldset>
	
<?php echo $this->Form->end(__('Submit'));?>
</div></div>
<div class="grid_4"> 
	<center><h3>Ayuda</h3></center>
	<ul><li>
	<p align="left">
		Todas las opciones de esta página son opcionales, (excepto nombre y apellido) llena sólo las que deseas mostrar en tu perfil público.
	</p></li><li>
	<p align="left">
		(&): Aun no tenemos una función para cargar imágenes en el servidor, por el momento estos campos sólo aceptan una dirección absoluta, ejemplo: http://pic.twitter.com/miimagen.png
	</p></li>
</div>
