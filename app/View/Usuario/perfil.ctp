﻿<?php
function existe ($parametro)
{
	if($parametro == null || $parametro == "")
	{
		return false;
	}	return true;
}
$u = $usuario['User'];
?>
<div id="main" class="wrapper">
    <section id="content" class="wide-content">
    	
		<div class="grid_4"><div class="cuadro_blanco"><div>
		<?php if(existe($u['web'])){
			echo '<h3><a target="_blank" href="'.$u['web'].'">'.$u['name'].' '.$u['lastname'].'</a></h3><hr />'; 
		}else{ ?>
		<h3><?php echo ($u['name']); ?> <?php echo ($u['lastname']); ?></h3>
		<hr />
		<?php 
		}
		if (existe($u['foto']))
			{echo ("<img align='left' style='margin-right: 10px;' width='100' src='".$u['foto']."' />");}
		if (existe($u['biografia']))
			{echo ("<p style='margin-left: 10px;'>".$u['biografia']."</p>");}
		echo ("<div class='clear'></div><hr />");
		if (existe($u['ciudad']) && existe($usuario['Country']['name']))
		{	echo ("<center>".$u['ciudad']." - ".$usuario['Country']['name']."</center>");
			if ($usuario['Country']['name']=='Venezuela')
			{
				echo ("<br /><center><img width='50' src='http://img4.wikia.nocookie.net/__cb20100106173704/lossimpson/es/images/thumb/7/7d/Bandera_de_Venezuela.png/100px-135,669,0,533-Bandera_de_Venezuela.png' /></center>");
			}
		}
		
		?>
		
		</div></div>
		<div class="cuadro_blanco"><div><h3><a href="/notas/de/<?php echo ($u['username']); ?>">Ver Notas Publicadas</a></h3></div></div>
		
		</div>
		<div class="grid_4"><div class="cuadro_blanco"><div>
		
		<h3>Datos Personales</h3><hr />
		<?php
		if (existe($u['titulo']) && existe($u['universidad']))
		{
			if(existe($u['logo_universidad']))
			{
				
				echo ("<img align='left' style='margin-right: 10px;' width='50' src='".$u['logo_universidad']."' />");
			}
			echo ("<p style='margin-left: 10px;'>".$u['titulo']);
			if(existe($u['pagina_universidad']))
			{
				echo' en <a target="_blank" href="'.$u['pagina_universidad'].'">'.$u['universidad'].'</a></p>';
			}
			else if(existe($u['universidad']))
			{
				echo ' en '.$u['universidad'].'</p>';
			}
		} ?>
		<ul>
		<?php
		if (existe($u['aficiones']))
		{
			echo'<li><u>Aficiones</u>: '.$u['aficiones'].'</li>';
		}
		if (existe($u['comidas']))
		{
			echo'<li><u>Comida preferida</u>: '.$u['comidas'].'</li>';
		}
		if (existe($u['equipos_favoritos']))
		{
			echo'<li><u>Equipos favoritos</u>: '.$u['equipos_favoritos'].'</li>';
		}
		if (existe($u['personal_adicional']))
		{
			echo'<li>'.$u['personal_adicional'].'</li>';
		}
		?>
		</ul>
		<?php
		if (existe($u['twitter']))
			{echo ("<hr /><center><a target='_blank' href='http://twitter.com/".$u['twitter']."'><img width='50' src='http://guzman6001.files.wordpress.com/2014/03/twitter.gif' /></a></center>");}
		
		?>
		</div></div></div>
		<div class="grid_4"><div class="cuadro_blanco"><div>
		<h3>Datos Profesionales</h3><hr />
		<?php
		
		if (existe($u['empresa']))
		{
			
			if(existe($u['logo_empresa']))
			{
				
				echo ("<img align='left' style='margin-right: 10px;' width='50' src='".$u['logo_empresa']."' />");
			}
			echo "<p style='margin-left: 10px;'>Trabaja en ";
			if(existe($u['pagina_empresa']))
			{
				echo'<a target="_blank" href="'.$u['pagina_empresa'].'">'.$u['empresa'].'</a>';
			}
			else
			{
				echo $u['empresa'];
			}
			if(existe($u['cargo']))
			{
				echo ' como '.$u['cargo'];
			}	
			echo '</p>';
		}
		
		if (existe($u['proyectos']))
		{
			echo'<li><u>Proyectos</u>: '.$u['proyectos'].'</li>';
		}
		if (existe($u['conocimientos']))
		{
			echo'<li><u>Conocimientos</u>: '.$u['conocimientos'].'</li>';
		}
		if (existe($u['profesional_adicional']))
		{
			echo'<li>'.$u['profesional_adicional'].'</li>';
		}
		if (existe($u['linkedin']))
		{
			echo "<hr /><center><a target='_blank' href='".$u['linkedin']."'><img width='50' src='http://guzman6001.files.wordpress.com/2014/03/linkedin.gif' /></a></center>";
		}
		?>
		</div></div></div>
		<div class="clear"><hr /><span style="font-size:small;">Nota: esta herramienta fue creada por <a href="http://laut.com.ve/usuario/perfil/guzman6001">Héctor Guzmán</a> como tarea para el curso <a href="https://www.miriadax.net/web/tecnicas-creatividad-3edicion" target="_blank">Técnicas de creatividad</a> en <a href="https://www.miriadax.net" target="_blank">Miriada X</a>.</span></div>
	</section>
</div>