﻿<div id="tope"><a name='enlace'><img src="/files/rdv2/images/cargando.gif" /></a><br />CARGANDO</div>
<?php
function esYoutube($url)
{
	return (strpos($url,'youtube.com') !== false);
}
function getVideoId($url)
{
  $url_string = parse_url($url, PHP_URL_QUERY);
  parse_str($url_string, $args);
  return isset($args['v']) ? $args['v'] : false;
}

function esImagen($url)
{
	if (
		terminaCon($url, ".png") ||
		terminaCon($url, ".PNG") ||
		
		terminaCon($url, ".gif") ||
		terminaCon($url, ".GIF") ||
		
		terminaCon($url, ".JPG") ||
		terminaCon($url, ".jpg")
		)
	{return true;}
	return false;
}
function terminaCon($haystack, $needle)
{
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}
?>
<!-- main content area -->   

<div id="main" class="wrapper">
    
    
<!-- content area -->    
<!-- header recent links: class="active" 
		<li class="active">
			<h3><span class="icon-tasks"></span>Tasks</h3>
-->
	<section id="content" class="wide-content">
    	<div class="grid_4">
		<!-- AGREGAR CATEGORIA -->
			<div class="formulario">
				<?php echo $this->Form->create('Category', array('url' => '/enlaces/nueva_categoria'));?>
				<fieldset>
					<legend><?php echo __('Agregar nueva categoría'); ?></legend>
					<?php 
					echo $this->Form->input('name', array('required' => true));
					 echo $this->Form->input('description'); ?>
				</fieldset>
				<?php echo $this->Form->end(__('Guardar'));?>
				</div>
			<div id="accordian">
			<ul>
				<?php 
				if(empty($enlacecategories))
				{echo"<li><h3>No se han guardado categorias todavía</h3></li>";}
				else
				{ 
				
					foreach ($enlacecategories as $category): 
					if (!empty($category['Enlace']))
					{					?>
					<li>
					<h3><?php echo ($category['Category']['name']); ?></h3>
					<ul>
						<?php 
						
						foreach ($category['Enlace'] as $enlace): 
						
						?>
						<li>
						<a title="<?php echo $enlace['title'] ?>"
						href="/enlaces/mostrar/<?php echo $enlace['id'] ?>">
							<?php echo $enlace['title'] ?>
						</a>
						</li>			
						<?php endforeach; ?>
						
					</ul>
					</li>
					<?php 
					}endforeach; ?>	
				<?php 
				} ?>
			</ul>
			</div>
        </div>
        
        <div class="grid_8">
			<div id="ENLACE_SELECCIONADO" class="enlace_seleccionado">
			<?php
			if (esImagen($link['Enlace']['url']))
						{
							$link['Enlace']['description']=$link['Enlace']['description']."<br /><center><img src='".$link['Enlace']['url']."'/></center>";
						}
						if (esYoutube($link['Enlace']['url']))
						{
							$link['Enlace']['description']=$link['Enlace']['description']."<br /><center><div class='contenedor'><iframe src='//www.youtube.com/embed/".getVideoId($link['Enlace']['url'])."' frameborder='0' allowfullscreen></iframe></div></center>";
						}
			?>
			<h3><?php echo h($link['Enlace']['title']); ?></h3>
			<?php echo ($link['Enlace']['description']); ?>
			<br/><center><a href='/enlaces/ver/<?php echo h($link['Enlace']['id']); ?>' class='button_verde'>Ver</a> <a target='_blank' href='<?php echo h($link['Enlace']['url']); ?>' class='button_verde'>Abrir</a> <a href='/enlaces/editar/<?php echo h($link['Enlace']['id']); ?>' class='button_verde'>Editar</a> <a href='/enlaces/borrar/<?php echo h($link['Enlace']['id']); ?>' class='button_rojo' onclick='return confirm(\"¿Seguro?\")'>Eliminar</a><br />&nbsp;</center>
			
			
			
			</div>
			<div class="formulario">
			<?php 
				if(!empty($enlacecategories))
				{ 
			
			echo $this->Form->create('Enlace', array('url' => '/enlaces/nuevo'));?>
				<fieldset>
					<legend><?php echo __('Agregar nuevo enlace'); ?></legend>
				<?php
					//echo $this->Form->input('title');
					echo $this->Form->input('category_id');
					echo $this->Form->input('title', array('required' => true));
					echo $this->Form->input('url');
					
					echo $this->Form->input('description');
					echo $this->Form->input('visibility_id');
				?>
				</fieldset>
			<?php echo $this->Form->end(__('Guardar'));
			}
			?>
		</div>
            
		</div>
		<br /><br />
		
	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->