﻿
<!-- main content area -->   
<div id="main" class="wrapper">
    
    
<!-- content area -->    
<!-- header recent links: class="active" 
		<li class="active">
			<h3><span class="icon-tasks"></span>Tasks</h3>
-->
	<section id="content" class="wide-content">
    	<div class="grid_4">
			<div id="accordian">
			<ul>
				<?php 
				if(empty($enlacecategories))
				{echo"<li><h3>No se han guardado categorias todavía</h3></li>";}
				else
				{ 
				
					foreach ($enlacecategories as $category): 
					if (!empty($category['Enlace']))
					{					?>
					<li>
					<h3><?php echo ($category['Category']['name']); ?></h3>
					<ul>
						<?php 
						
						foreach ($category['Enlace'] as $enlace): 
						
						?>
						<li>
						<a title="<?php echo $enlace['title'] ?>"
						href="/enlaces/mostrar/<?php echo $enlace['id'] ?>">
							<?php echo $enlace['title'] ?>
						</a>
						</li>			
						<?php endforeach; ?>
						
					</ul>
					</li>
					<?php 
					}endforeach; ?>	
				<?php 
				} ?>
			</ul>
			</div>
        </div>
        
        <div class="grid_8">
			<div id="ENLACE_SELECCIONADO"></div>
			<div class="formulario">
			<?php 
				if(!empty($enlacecategories))
				{ 
			
			echo $this->Form->create('Enlace');?>
				<fieldset>
					<legend><?php echo __('Actualizar enlace'); ?></legend>
				<?php
					echo $this->Form->input('id');
					echo $this->Form->input('category_id');
					echo $this->Form->input('title', array('required' => true));
					echo $this->Form->input('url', 
					array(
						'required' => true,
						//'div' => false,
						'label' => array(
							'text'=>'Dirección (URL)<span style="color:red">*</span>',
							'class'=>'input text required'
							)
						));
					
					echo $this->Form->input('description');
					echo $this->Form->input('visibility_id');
				?>
				</fieldset>
			<?php echo $this->Form->end(__('Guardar'));
			}
			?>
		</div>
            <!-- AGREGAR CATEGORIA -->
			<div class="formulario">
				<?php echo $this->Form->create('Category', array('url' => '/enlaces/nueva_categoria'));?>
				<fieldset>
					<legend><?php echo __('Agregar nueva categoría'); ?></legend>
					<?php 
					echo $this->Form->input('name', array('required' => true));
					 echo $this->Form->input('description'); ?>
				</fieldset>
				</div>
				<div id="loading" style="display:none;"><h2>Guargando...</h2></div>
				<div id="result" style="display:none;"><h2>Categoría agregada.</h2></div>
		</div>
		<br /><br />
		
	</section><!-- #end content area -->   
    <!-- end columns demo -->  
    
      
  </div><!-- #end div #main .wrapper -->