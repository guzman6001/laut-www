<?php
App::uses('AppController', 'Controller');

class CategoriesController extends AppController 
{

	public function index() 
	{
		$this->set('title_for_layout', 'Categorias de Enlaces');
		$this->Category->recursive = 0;
		$this->set('categories', $this->paginate());
	}


	public function add() 
	{
		$this->set('title_for_layout', 'Categorias de Enlaces');
		if ($this->request->is('post')) 
		{
			$this->Category->create();
			if ($this->Category->save($this->request->data)) 
			{
				$this->Session->setFlash(__('The category has been saved'));
				$this->redirect(array('action' => 'index'));
			}
			else 
			{
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Categorias de Enlaces');
		$this->Category->id = $id;
		if (!$this->Category->exists()) 
		{
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			if ($this->Category->save($this->request->data)) 
			{
				$this->Session->setFlash(__('The category has been saved'));
				$this->redirect(array('action' => 'index'));
			} 
			else 
			{
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'));
			}
		} 
		else 
		{
			$this->request->data = $this->Category->read(null, $id);
		}
	}

	public function delete($id = null) 
	{
		if (!$this->request->is('post')) 
		{
			throw new MethodNotAllowedException();
		}
		$this->Category->id = $id;
		if (!$this->Category->exists()) 
		{
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->Category->delete()) 
		{
			$this->Session->setFlash(__('Category deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Category was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
