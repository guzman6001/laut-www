<?php
App::uses('AppController', 'Controller');


class LinksController extends AppController 
{
	var $uses = array('Category','Link');

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index');
	}
	
	public function index() 
	{
		$this->set('title_for_layout', 'Enlaces');
		if ($this->RequestHandler->isRss() ) 
		{
			$links = $this->Link->find('all', 
				array('limit' => 20, 
					'order' => 'Link.created DESC'));
	        	$this->set(compact('links'));
		}
		else
		{
			$this->Category->recursive = 1;
			$this->set('categories', $this->paginate());
		}
	}

	public function add() 
	{
		$this->set('title_for_layout', 'Enlaces');
		if ($this->request->is('post')) 
		{
			$this->Link->create();
			if ($this->Link->save($this->request->data)) 
			{
				$this->Session->setFlash(__('The link has been saved'));
				$this->redirect(array('action' => 'index'));
			} 
			else
			{
				$this->Session->setFlash(__('The link could not be saved. Please, try again.'));
			}
		}
		$categories = $this->Link->Category->find('list');
		$tags = $this->Link->Tag->find('list');
		$this->set(compact('categories', 'tags'));
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Enlaces');
		$this->Link->id = $id;
		if (!$this->Link->exists()) 
		{
			throw new NotFoundException(__('Invalid link'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			if ($this->Link->save($this->request->data)) 
			{
				$this->Session->setFlash(__('The link has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The link could not be saved. Please, try again.'));
			}
		} 
		else
		{
			$this->request->data = $this->Link->read(null, $id);
		}
		$categories = $this->Link->Category->find('list');
		$tags = $this->Link->Tag->find('list');
		$this->set(compact('categories', 'tags'));
	}

	public function delete($id = null) 
	{
		if (!$this->request->is('post')) 
		{
			throw new MethodNotAllowedException();
		}
		$this->Link->id = $id;
		if (!$this->Link->exists()) 
		{
			throw new NotFoundException(__('Invalid link'));
		}
		if ($this->Link->delete()) 
		{
			$this->Session->setFlash(__('Link deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Link was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
