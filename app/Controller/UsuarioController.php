<?php
App::uses('AppController', 'Controller');

class UsuarioController extends AppController 
{	
	var $uses = array('User','Lation');
	var $helpers = array('Html', 'Form', 'Captcha');
	var $components = array('Captcha'=> 
		  array('captchaType'=>'image', 
		  'jquerylib'=>true, 
		  'modelName'=>'User', 
		  'fieldName'=>'captcha')
		  ); //load it
  
	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('acceder','registro','captcha','perfil');
		$this->Auth->autoRedirect = false;

		$this->Auth->loginRedirect = array('controller' => 'p', 'action' => 'inicio');
		$this->Auth->logoutRedirect = array('controller' => 'pages', 'action' => 'index');

		$this->Auth->loginError = "No hay coincidencia entre el usuario y contraseña.";
		$this->Auth->authError = "Sin acceso.";
	}
	
	public function index() 
	{
		$this->redirect(array('action' => 'index','controller'=>'p'));
	}

	public function view($id = null) 
	{
		$this->User->id = $id;
		if (!$this->User->exists()) 
		{
			throw new NotFoundException(__('Usuario incorrecto'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */

	public function editar_perfil()
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Editar mi perfil');
		$this->User->id = $user_id;
		if (!$this->User->exists()) 
		{
			throw new NotFoundException(__('Usuario incorrecto'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			if ($this->User->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Perfil actualizado'), 'default', array('class' => 'notice success'));
			} 
			else 
			{
				$this->Session->setFlash(__('Perfil no guardado.'));
			}
		} 
		else 
		{
			$this->request->data = $this->User->read(null, $user_id);
		}
		$countries = $this->User->Country->find('list');
		$this->set(compact('countries'));
	}
	
	function perfil($username = null)
	{
		if ($username==null || $username=="")
		{
			$user_id = $this->Auth->user('id');
			$this->set('title_for_layout', 'Visualizar mi perfil');
			$this->User->id = $user_id;
			if (!$this->User->exists()) 
			{
				throw new NotFoundException(__('Usuario inválido'));
			}
			$this->set('usuario', $this->User->read(null, $user_id));
		}
		else
		{
			$this->set('usuario', $this->User->find('first', array(
				'conditions' => array('User.username' => $username)
			)));
		}
	}
 
	public function edit($id = null) 
	{
		$this->User->id = $id;
		if (!$this->User->exists()) 
		{
			throw new NotFoundException(__('Usuario incorrecto'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			if ($this->User->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Usuario guardado'), 'default', array('class' => 'notice success'));
				$this->redirect(array('action' => 'index'));
			} 
			else 
			{
				$this->Session->setFlash(__('The usuario no guardado.'));
			}
		} 
		else 
		{
			$this->request->data = $this->User->read(null, $id);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

	public function delete($id = null) 
	{
		if (!$this->request->is('post')) 
		{
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) 
		{
			throw new NotFoundException(__('Usuario incorrecto'));
		}
		if ($this->User->delete()) 
		{
			$this->Session->setFlash(__('Usuario eliminado'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Usuario no eliminado'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function acceder() 
	{
		$this->set('title_for_layout', 'Acceder');
		
		if ($this->Auth->login()) 
		{
			return $this->redirect($this->Auth->redirect());
		}
		else 
		{
			$this->Session->setFlash(__('Usuario y contraseña no coinciden'), 'default', array(), 'auth');
		}
	}
	
	public function registro() 
	{
		$this->set('title_for_layout', 'Registro');
		$this->Captcha = $this->Components->load('Captcha', array('captchaType'=>'math', 'jquerylib'=>true, 'modelName'=>'User', 'fieldName'=>'captcha')); //load it
		if ($this->request->is('post'))
		{
			$this->User->setCaptcha($this->Captcha->getVerCode()); // catpcha
			$this->User->set($this->request->data); //captcha
			
			if($this->User->validates())	
			{ 
			
				if ($this->request->data['User']['password'] == $this->request->data['User']['password_confirm'])
				{
					if (strlen($this->request->data['User']['password'])>7)
					{
						$this->User->create();
						if ($this->User->save($this->request->data)) 
						{
							$this->Session->setFlash('Se ha completado el registro', 'default', array('class' => 'notice success'));
							$this->redirect(array('controller' => 'sitio','action' => 'index'));
						}
						else 
						{
							$this->Session->setFlash(__('El usuario no pudo ser guardado. Verifique los datos e intente nuevamente', true));
							unset($this->request->data['User']['password']);
							unset($this->request->data['User']['password_confirm']);
						}
					}
					else
					{
						$this->Session->setFlash(__('La contraseña debe tener por lo menos 8 caracteres. Intente de nuevo', true));
						unset($this->request->data['User']['password']);
						unset($this->request->data['User']['password_confirm']);
					}
				}
				else
				{
					$this->Session->setFlash(__($this->request->data['User']['password_confirm'].'Las contraseñas no coinciden. Intente de nuevo', true));
					unset($this->request->data['User']['password']);
					unset($this->request->data['User']['password_confirm']);
				}
			}	else	{ //or
				$this->Session->setFlash('Falló la validación del captcha', 'default', array('class' => 'cake-error'));
				//pr($this->Signup->validationErrors);
				//something do something else
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
		$countries = $this->User->Country->find('list');
		$this->set(compact('countries'));
		
	}
	
	function ver()
	{
		$this->set('title_for_layout', 'Página personal');
		$lations = $this->Lation->find('all', 
				array('limit' => 10, 
					'order' => 'Lation.created DESC'));
	    $this->set(compact('lations'));
	}
	
	public function salir() 
	{
		$this->redirect($this->Auth->logout());
	}
	
	public function captcha()	{
		$this->autoRender = false;
		$this->layout='ajax';
		if(!isset($this->Captcha))	{ //if Component was not loaded throug $components array()
			$this->Captcha = $this->Components->load('Captcha', array(
				'width' => 150,
				'height' => 50,
				'theme' => 'random', //possible values : default, random ; No value means 'default'
			)); //load it
			}
		$this->Captcha->create();
	}
}
