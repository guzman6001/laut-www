<?php

class NotasController extends AppController 
{
	var $name = 'Notas'; 
	var $uses = array('Nota','Category','Visibility','User');
	public $paginate = array(
        	'limit' => 8,
        	'order' => array('Nota.created' => 'desc')
    	);
		
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('ver','de','rss');
	}
	// Métodos agregados por guzman6001:
	function index()
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Notas');
				
		$categories = $this->Nota->Category->find('list',
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
		$this->Category->recursive = 1;
		$this->Visibility->recursive = 0;
		$notacategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
		$visibilities = $this->Visibility->find('list');
		$this->set('notas', $this->paginate
		( array('Nota.user_id' => $user_id
		)));
	    $this->set(compact('categories','visibilities','notacategories'));
	}
	
	function de($usuario = null)
	{
		if(isset($usuario))
		{
			$user_id = $this->Auth->user('id');
			$this->set('title_for_layout', 'Notas de '.$usuario);
				
			$usuario_completo = $this->User->find('first', array(
				'conditions' => array('User.username' => $usuario)));
			/*
			$this->Nota->user_id = ;
			$this->Nota->visibility_id = 1;
			$this->Nota->recursive=1;*/
			$this->Category->recursive = 1;
			$notacategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $usuario_completo['User']['id']),
					'order' => 'Category.name ASC'
					));
			
					$this->paginate['conditions']=array('Nota.visibility_id ' => 1,
				'Nota.user_id' => $usuario_completo['User']['id'],
				);
		$this->set('notas', $this->paginate()); 
	    $this->set(compact('notacategories','usuario_completo'));
		}
		else
		{
			$this->Session->setFlash(__('Se debe indicar un usuario'), 'default', array('class' => 'notice error'));
				$this->redirect(array('action' => 'index','controller' => 'p'));
		}
		
	}
	
	public function nuevo() 
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Notas');
		if ($this->request->is('post')) 
		{
			$this->Nota->create();
			$this->request->data['Nota']['user_id'] = $this->Auth->user('id');
			if ($this->Nota->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Nota guardada exitosamente'), 'default', array('class' => 'notice success'));
				$this->redirect(array('action' => 'index'));
			} 
			else
			{
				$this->Session->setFlash(__('El nota no pudo ser guardado '));
				$this->redirect(array('action' => 'index'));
			}
		}
		
		
		$categories = $this->Nota->Category->find('list',
					array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
					$this->Category->recursive = 1;
		$notacategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
	    $visibilities = $this->Visibility->find('list');
	    $this->set(compact('categories','visibilities','notacategories'));
	}
	
	public function nueva_categoria() 
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Notas');
		if ($this->request->is('post')) 
		{
			$this->Category->create();
			$this->request->data['Category']['user_id'] = $this->Auth->user('id');
			if ($this->Category->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Categoría guardada satisfactoriamente'), 'default', array('class' => 'notice success'));
				$this->redirect(array('action' => 'index'));
			} 
			else
			{
				$this->Session->setFlash(__('La categoría no pudo ser guardada'));
				$this->redirect(array('action' => 'index'));
			}
		}
		
		
		$categories = $this->Nota->Category->find('list',
					array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
					$this->Category->recursive = 1;
		$notacategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
	     $visibilities = $this->Visibility->find('list');
	    $this->set(compact('categories','visibilities','notacategories'));
	}
	function ver($id = null)
	{
		//$this->layout = 'view_link';
		$this->set('title_for_layout', 'Nota');
		$this->Nota->id = $id;
		if (!$this->Nota->exists()) 
		{
			throw new NotFoundException(__('Nota inválido'));
		}
		$this->set('link', $this->Nota->read(null, $id));
		
		$nota=$this->Nota->read(null, $id);
		$this->Category->recursive = 1;
			$notacategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $nota['Nota']['user_id']),
					'order' => 'Category.name ASC'
					));
					 $this->set(compact('notacategories'));
	}
	
	public function editar($id = null) 
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Editar nota');
		$this->Nota->id = $id;
		if (!$this->Nota->exists()) 
		{
			throw new NotFoundException(__('Nota inválido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			if ($this->Nota->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Nota guardada satisfactoriamente'), 'default', array('class' => 'notice success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La categoría no pudo ser guardada'));
			}
		} 
		else
		{
			$this->request->data = $this->Nota->read(null, $id);
		}
		
		$categories = $this->Nota->Category->find('list',
					array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
					$this->Category->recursive = 1;
		$notacategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
		
	    $visibilities = $this->Visibility->find('list');
		$this->set('notas', $this->paginate());
	    $this->set(compact('categories','visibilities','notacategories'));
	}
	
	public function borrar($id = null) 
	{
		if (!$this->request->is('get')) 
		{
			throw new MethodNotAllowedException();
		}
		$this->Nota->id = $id;
		if (!$this->Nota->exists()) 
		{
			throw new NotFoundException(__('Nota inexistente o inaccesible'));
		}
		if ($this->Nota->delete()) 
		{
			$this->Session->setFlash(__('Nota eliminado'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('El nota no pudo ser eliminado'));
		$this->redirect(array('action' => 'index'));
	}
	
	
	function index_old()
	{
		$this->set('title_for_layout', 'Notas');
		$links = $this->Link->find('all', 
			array('limit' => 10, 
			'order' => 'Link.created DESC'));
		$videos = $this->Video->find('all', 
			array('limit' => 1, 
			'order' => 'Video.created DESC'));
		$images = $this->Image->find('all', 
			array('limit' => 1, 
			'order' => 'Image.created DESC'));
		$posts = $this->Visibility->find('all', 
			array('limit' => 30, 
			'order' => 'Visibility.created DESC'));
		$microposts = $this->Micropost->find('all', 
			array('limit' => 8, 
			'order' => 'Micropost.created DESC'));
		$comics = $this->Comic->find('all',  
			array('limit' => 1, 
			'order' => 'Comic.created DESC'));
		$fotos = $this->Foto->find('all',  
			array('limit' => 1, 
			'order' => 'Foto.created DESC'));	
			
			
			
	        $this->set(compact('links','videos','images','posts','microposts','comics','fotos'));
	}
	function construccion()
	{
	$this->set('title_for_layout', 'Pgina en construccin');
	}
	function buscar(){$this->set('title_for_layout', 'Buscar en el Sitio');}
	function plantilla(){$this->set('title_for_layout', 'Plantilla');}
}
