<?php

class RandomController extends AppController 
{
	var $name = 'Random';
	var $uses = array('Link');
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('*');
	}
	// Métodos agregados por guzman6001:

	function index()
	{
		$this->set('title_for_layout', 'Cosas Random en la vida');
	}
	function michel2012()
	{
		$this->set('title_for_layout', 'Regalo de Cumpleaños - Michel 2012');
	}
}
