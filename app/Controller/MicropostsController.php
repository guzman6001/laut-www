<?php
App::uses('AppController', 'Controller');

class MicropostsController extends AppController 
{
	public $paginate = array
	(
        	'limit' => 100,
        	'order' => array('Micropost.created' => 'desc')
    	);

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}

	public function index() 
	{
		$this->set('title_for_layout', 'Nanoblog');
		if ($this->RequestHandler->isRss()) 
		{
			$microposts = $this->Micropost->find('all', 
				array('limit' => 20, 
					'order' => 'Micropost.created DESC'));
	        	$this->set(compact('microposts'));
		}
		else
		{
			$this->Micropost->recursive = 0;
			$this->set('microposts', $this->paginate());
		}
	}

	public function view($id = null) 
	{
		$this->set('title_for_layout', 'Nanoblog');
		$this->Micropost->id = $id;
		if (!$this->Micropost->exists()) 
		{
			throw new NotFoundException(__('Invalid micropost'));
		}
		$this->set('micropost', $this->Micropost->read(null, $id));
	}
	
	public function add() 
	{
		$this->set('title_for_layout', 'Nanoblog');
		if ($this->request->is('post')) 
		{
			$this->Micropost->create();
			if ($this->Micropost->save($this->request->data)) 
			{
				$this->Session->setFlash(__('The micropost has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The micropost could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Nanoblog');
		$this->Micropost->id = $id;
		if (!$this->Micropost->exists()) {
			throw new NotFoundException(__('Invalid micropost'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Micropost->save($this->request->data)) {
				$this->Session->setFlash(__('The micropost has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The micropost could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Micropost->read(null, $id);
		}
	}

	public function delete($id = null) 
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Micropost->id = $id;
		if (!$this->Micropost->exists()) {
			throw new NotFoundException(__('Invalid micropost'));
		}
		if ($this->Micropost->delete()) {
			$this->Session->setFlash(__('Micropost deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Micropost was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
