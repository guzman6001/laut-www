<?php

class PController extends AppController 
{
	var $name = 'P';
	var $uses = array('Enlace','Nota','Category');
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index'); 
	}
	function index()
	{
		$this->set('title_for_layout', 'Inicio');
		
	}
	function inicio()
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Mi Inicio');
		$enlaces = $this->Enlace->find('all', 
				array('limit' => 10, 
					'conditions' => array('Enlace.user_id' => $user_id),
					'order' => 'Enlace.created DESC'));
		$notas = $this->Nota->find('all', 
				array('limit' => 10, 
					'conditions' => array('Nota.user_id' => $user_id),
					'order' => 'Nota.created DESC'));
		$categories = $this->Enlace->Category->find('list',
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
		$this->Category->recursive = 1;
		$enlacecategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
	    $this->set(compact('categories','enlacecategories','enlaces','notas'));
	}
	function index2()
	{
		$this->set('title_for_layout', 'Inicio');
		$links = $this->Link->find('all', 
			array('limit' => 10, 
			'order' => 'Link.created DESC'));
		$videos = $this->Video->find('all', 
			array('limit' => 1, 
			'order' => 'Video.created DESC'));
		$images = $this->Image->find('all', 
			array('limit' => 1, 
			'order' => 'Image.created DESC'));
		$posts = $this->Post->find('all', 
			array('limit' => 30, 
			'order' => 'Post.created DESC'));
		$microposts = $this->Micropost->find('all', 
			array('limit' => 8, 
			'order' => 'Micropost.created DESC'));
		$comics = $this->Comic->find('all',  
			array('limit' => 1, 
			'order' => 'Comic.created DESC'));
		$fotos = $this->Foto->find('all',  
			array('limit' => 1, 
			'order' => 'Foto.created DESC'));	
			
			
			
	        $this->set(compact('links','videos','images','posts','microposts','comics','fotos'));
	}
	function construccion(){$this->set('title_for_layout', 'Página en construcción');}
	function buscar(){$this->set('title_for_layout', 'Buscar en el Sitio');}
	function eventos(){$this->set('title_for_layout', 'Eventos Culturales');}
}
