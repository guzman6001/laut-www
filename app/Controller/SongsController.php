﻿<?php
App::uses('AppController', 'Controller');

class SongsController extends AppController 
{
	public $paginate = array(
        	'limit' => 100,
        	'order' => array('Song.rating' => 'desc')
    	);

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index','view','playlist');
	}

	public function index() 
	{
		$this->set('title_for_layout', 'Música Libre');
		if ($this->RequestHandler->isRss() ) 
		{
			$songs = $this->Song->find('all', 
				array('limit' => 20, 
					'order' => 'Song.created DESC'));
	        	$this->set(compact('songs'));
		}
		else
		{
			$this->Song->recursive = 0;
			$this->set('songs', $this->paginate());
			
			$sings = $this->Song->find('all', 
				array('limit' => 200, 
					'group' => 'Song.playlistslug'));
	        	$this->set(compact('sings'));
		}
		
		
	}
	public function playlist($playlistslug = null) 
	{
		$this->set('title_for_layout', 'Música Libre');

		$this->Song->recursive = 0;
		$this->set('songs', $this->paginate
		(
			'Song',
			array('Song.playlistslug' => $playlistslug)
		));
		
		$sings = $this->Song->find('all', 
				array('limit' => 200, 
					'group' => 'Song.playlistslug'));
	        	$this->set(compact('sings'));
	}
	public function index2() 
	{
		$this->set('title_for_layout', 'Música Libre');
		$this->Song->recursive = 0;
		$this->set('songs', $this->paginate());
	}
	
	public function view($id = null) 
	{
		$this->set('title_for_layout', 'Música Libre');
		$this->Song->id = $id;
		if (!$this->Song->exists()) {
			throw new NotFoundException(__('Invalid song'));
		}
		$this->set('song', $this->Song->read(null, $id));
	}
	
	public function viewcircle($id = null) 
	{
		$this->set('title_for_layout', 'Música Libre');
		$this->Song->id = $id;
		if (!$this->Song->exists()) {
			throw new NotFoundException(__('Invalid song'));
		}
		$this->set('song', $this->Song->read(null, $id));
	}

	public function add() 
	{
		$this->set('title_for_layout', 'Música Libre');
		if ($this->request->is('post')) {
			$this->Song->create();
			if ($this->Song->save($this->request->data)) {
				$this->Session->setFlash(__('The song has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The song could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Música Libre');
		$this->Song->id = $id;
		if (!$this->Song->exists()) {
			throw new NotFoundException(__('Invalid song'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Song->save($this->request->data)) {
				$this->Session->setFlash(__('The song has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The song could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Song->read(null, $id);
		}
	}
	
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Song->id = $id;
		if (!$this->Song->exists()) {
			throw new NotFoundException(__('Invalid song'));
		}
		if ($this->Song->delete()) {
			$this->Session->setFlash(__('Song deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Song was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
