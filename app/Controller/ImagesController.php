<?php
App::uses('AppController', 'Controller');

class ImagesController extends AppController 
{
	public $paginate = array(
        	'limit' => 7,
        	'order' => array('Image.created' => 'desc')
    	);

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}

	public function index() 
	{
		$this->set('title_for_layout', 'Images');
		if ($this->RequestHandler->isRss() ) 
		{
			$images = $this->Image->find('all', 
				array('limit' => 20, 
					'order' => 'Image.created DESC'));
	        	$this->set(compact('images'));
		}
		else
		{
			$this->Image->recursive = 0;
			$this->set('images', $this->paginate());
		}
	}

	public function view($id = null) 
	{
		$this->set('title_for_layout', 'Images');
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Invalid image'));
		}
		$this->set('image', $this->Image->read(null, $id));
	}

	public function add() 
	{
		$this->set('title_for_layout', 'Images');
		if ($this->request->is('post')) {
			$this->Image->create();
			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Images');
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Invalid image'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Image->save($this->request->data)) {
				$this->Session->setFlash(__('The image has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The image could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Image->read(null, $id);
		}
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Invalid image'));
		}
		if ($this->Image->delete()) {
			$this->Session->setFlash(__('Image deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Image was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
