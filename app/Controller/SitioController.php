<?php

class SitioController extends AppController 
{
	var $name = 'Sitio'; 
	var $uses = array('Link','Video','Foto','Image','Post','Micropost','Quote','Comic');
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('*');
	}
	// Métodos agregados por guzman6001:

	function index()
	{
		$this->set('title_for_layout', 'Inicio');
		$links = $this->Link->find('all', 
			array('limit' => 10, 
			'order' => 'Link.created DESC'));
		$videos = $this->Video->find('all', 
			array('limit' => 1, 
			'order' => 'Video.created DESC'));
		$images = $this->Image->find('all', 
			array('limit' => 1, 
			'order' => 'Image.created DESC'));
		$posts = $this->Post->find('all', 
			array('limit' => 30, 
			'order' => 'Post.created DESC'));
		$microposts = $this->Micropost->find('all', 
			array('limit' => 8, 
			'order' => 'Micropost.created DESC'));
		$comics = $this->Comic->find('all',  
			array('limit' => 1, 
			'order' => 'Comic.created DESC'));
		$fotos = $this->Foto->find('all',  
			array('limit' => 1, 
			'order' => 'Foto.created DESC'));	
			
			
			
	        $this->set(compact('links','videos','images','posts','microposts','comics','fotos'));
	}
	function construccion(){$this->set('title_for_layout', 'Página en construcción');}
}
