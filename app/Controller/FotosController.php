<?php
App::uses('AppController', 'Controller');

class FotosController extends AppController 
{
	public $paginate = array(
        	'limit' => 7,
        	'order' => array('Foto.created' => 'desc')
    	);

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index','view','album');
	}

	public function index() 
	{
		$this->set('title_for_layout', 'Fotos');
		if ($this->RequestHandler->isRss() ) 
		{
			$fotos = $this->Foto->find('all', 
				array('limit' => 20, 
					'order' => 'Foto.created DESC'));
	        	$this->set(compact('fotos'));
		}
		else
		{
			$this->Foto->recursive = 0;
			$this->set('fotos', $this->paginate());
		}
	}

	public function view($id = null) 
	{
		$this->set('title_for_layout', 'Fotos');
		$this->Foto->id = $id;
		if (!$this->Foto->exists()) {
			throw new NotFoundException(__('Invalid foto'));
		}
		$this->set('foto', $this->Foto->read(null, $id));
	}
	
	public function album($albumslug = null) 
	{
		$this->set('title_for_layout', 'Comics');

		$this->Foto->recursive = 0;
		$this->set('fotos', $this->paginate
		(
			'Foto',
			array('Foto.albumslug' => $albumslug)
		));
	}

	public function add() 
	{
		$this->set('title_for_layout', 'Fotos');
		if ($this->request->is('post')) {
			$this->Foto->create();
			if ($this->Foto->save($this->request->data)) {
				$this->Session->setFlash(__('The foto has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The foto could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Fotos');
		$this->Foto->id = $id;
		if (!$this->Foto->exists()) {
			throw new NotFoundException(__('Invalid foto'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Foto->save($this->request->data)) {
				$this->Session->setFlash(__('The foto has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The foto could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Foto->read(null, $id);
		}
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Foto->id = $id;
		if (!$this->Foto->exists()) {
			throw new NotFoundException(__('Invalid foto'));
		}
		if ($this->Foto->delete()) {
			$this->Session->setFlash(__('Foto deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Foto was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
