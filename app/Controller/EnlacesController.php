﻿<?php

class EnlacesController extends AppController 
{
	var $name = 'Enlaces'; 
	var $uses = array('Enlace','Category','Visibility','User');
	
	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('ver');
	}
	// Métodos agregados por guzman6001:
	function index()
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Enlaces');
				
		$categories = $this->Enlace->Category->find('list',
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
		$this->Category->recursive = 1;
		$this->Visibility->recursive = 0;
		$enlacecategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
		$visibilities = $this->Visibility->find('list');
	    $this->set(compact('categories','visibilities','enlacecategories'));
	}
	
	function mostrar($id = null)
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Enlaces');
				
		$categories = $this->Enlace->Category->find('list',
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
		$this->Category->recursive = 1;
		$this->Visibility->recursive = 0;
		$enlacecategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
		$visibilities = $this->Visibility->find('list');
		
		$this->Enlace->id = $id;
		if (!$this->Enlace->exists()) 
		{
			throw new NotFoundException(__('Enlace inválido'));
		}
		$this->set('link', $this->Enlace->read(null, $id));
		
	    $this->set(compact('categories','visibilities','enlacecategories'));
	}
	
	public function nuevo() 
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Enlaces');
		if ($this->request->is('post')) 
		{
			$this->Enlace->create();
			$this->request->data['Enlace']['user_id'] = $this->Auth->user('id');
			if ($this->Enlace->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Enlace guardado satisfactoriamente'), 'default', array('class' => 'notice success'));
				$this->redirect(array('action' => 'index'));
			} 
			else
			{
				$this->Session->setFlash(__('El enlace no pudo ser guardado '));
				$this->redirect(array('action' => 'index'));
			}
		}
		
		
		$categories = $this->Enlace->Category->find('list',
					array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
					$this->Category->recursive = 1;
		$enlacecategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
	    $visibilities = $this->Visibility->find('list');
	    $this->set(compact('categories','visibilities','enlacecategories'));
	}
	
	public function nueva_categoria() 
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Enlaces');
		if ($this->request->is('post')) 
		{
			$this->Category->create();
			$this->request->data['Category']['user_id'] = $this->Auth->user('id');
			if ($this->Category->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Categoría guardado satisfactoriamente'), 'default', array('class' => 'notice success'));
				$this->redirect(array('action' => 'index'));
			} 
			else
			{
				$this->Session->setFlash(__('La categoría no pudo ser guardada'));
				$this->redirect(array('action' => 'index'));
			}
		}
		
		
		$categories = $this->Enlace->Category->find('list',
					array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
					$this->Category->recursive = 1;
		$enlacecategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
	     $visibilities = $this->Visibility->find('list');
	    $this->set(compact('categories','visibilities','enlacecategories'));
	}
	function ver($id = null)
	{
		$this->layout = 'view_link';
		$this->set('title_for_layout', 'Enlace (Vista previa)');
		$this->Enlace->id = $id;
		if (!$this->Enlace->exists()) 
		{
			throw new NotFoundException(__('Enlace inválido'));
		}
		$this->set('link', $this->Enlace->read(null, $id));
	}
	
	public function editar($id = null) 
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Editar enlace');
		$this->Enlace->id = $id;
		if (!$this->Enlace->exists()) 
		{
			throw new NotFoundException(__('Enlace inválido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) 
		{
			if ($this->Enlace->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Enlace guardado satisfactoriamente'), 'default', array('class' => 'notice success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('La categoría no pudo ser guardada'));
			}
		} 
		else
		{
			$this->request->data = $this->Enlace->read(null, $id);
		}
		
		$categories = $this->Enlace->Category->find('list',
					array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'));
					$this->Category->recursive = 1;
		$enlacecategories = $this->Category->find('all', 
				array(
					'conditions' => array('Category.user_id' => $user_id),
					'order' => 'Category.name ASC'
					));
		
	    $visibilities = $this->Visibility->find('list');
	    $this->set(compact('categories','visibilities','enlacecategories'));
	}
	
	public function borrar($id = null) 
	{
		if (!$this->request->is('get')) 
		{
			throw new MethodNotAllowedException();
		}
		$this->Enlace->id = $id;
		if (!$this->Enlace->exists()) 
		{
			throw new NotFoundException(__('Enlace inexistente o inaccesible'));
		}
		if ($this->Enlace->delete()) 
		{
			$this->Session->setFlash(__('Enlace eliminado'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('El enlace no pudo ser eliminado'));
		$this->redirect(array('action' => 'index'));
	}
	
	
	function index_old()
	{
		$this->set('title_for_layout', 'Enlaces');
		$links = $this->Link->find('all', 
			array('limit' => 10, 
			'order' => 'Link.created DESC'));
		$videos = $this->Video->find('all', 
			array('limit' => 1, 
			'order' => 'Video.created DESC'));
		$images = $this->Image->find('all', 
			array('limit' => 1, 
			'order' => 'Image.created DESC'));
		$posts = $this->Visibility->find('all', 
			array('limit' => 30, 
			'order' => 'Visibility.created DESC'));
		$microposts = $this->Micropost->find('all', 
			array('limit' => 8, 
			'order' => 'Micropost.created DESC'));
		$comics = $this->Comic->find('all',  
			array('limit' => 1, 
			'order' => 'Comic.created DESC'));
		$fotos = $this->Foto->find('all',  
			array('limit' => 1, 
			'order' => 'Foto.created DESC'));	
			
			
			
	        $this->set(compact('links','videos','images','posts','microposts','comics','fotos'));
	}
	function construccion()
	{
	$this->set('title_for_layout', 'Pgina en construccin');
	}
	function buscar(){$this->set('title_for_layout', 'Buscar en el Sitio');}
	function plantilla(){$this->set('title_for_layout', 'Plantilla');}
}
