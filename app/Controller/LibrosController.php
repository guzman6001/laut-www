<?php

class LibrosController extends AppController 
{
	var $name = 'Libros'; 
	var $uses = array('Libro','Category','Visibility','User');
	
	function index()
	{
		$user_id = $this->Auth->user('id');
		$this->set('title_for_layout', 'Libros');
				
		$categories = $this->Category->find('all',
				array(
					'conditions' => array('Category.tipo_id' => 2),
					'order' => 'Category.name ASC'));
		$this->Visibility->recursive = 0;
		$visibilities = $this->Visibility->find('list');
	    $this->set(compact('visibilities','categories'));
	}
}