<?php
App::uses('AppController', 'Controller');

class QuotesController extends AppController 
{
	public $paginate = array
	(
        	'limit' => 40,
        	'order' => array('Quote.created' => 'desc')
    	);

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index','view');
	}

	public function index() 
	{
		$this->set('title_for_layout', 'Quotes');
		if ($this->RequestHandler->isRss() ) 
		{
			$quotes = $this->Quote->find('all', 
				array('limit' => 5, 
					'order' => 'Quote.created DESC'));
	        	$this->set(compact('quotes'));
		}
		else
		{		
			$this->Quote->recursive = 0;
			$this->set('quotes', $this->paginate());
		}
	}

	public function view($id = null) 
	{
		$this->set('title_for_layout', 'Quotes');
		$this->Quote->id = $id;
		if (!$this->Quote->exists()) {
			throw new NotFoundException(__('Invalid quote'));
		}
		$this->set('quote', $this->Quote->read(null, $id));
	}

	public function add() 
	{
		$this->set('title_for_layout', 'Quotes');
		if ($this->request->is('post')) {
			$this->Quote->create();
			if ($this->Quote->save($this->request->data)) {
				$this->Session->setFlash(__('The quote has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The quote could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Quotes');
		$this->Quote->id = $id;
		if (!$this->Quote->exists()) {
			throw new NotFoundException(__('Invalid quote'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Quote->save($this->request->data)) {
				$this->Session->setFlash(__('The quote has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The quote could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Quote->read(null, $id);
		}
	}

	public function delete($id = null) 
	{
		$this->set('title_for_layout', 'Quotes');
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Quote->id = $id;
		if (!$this->Quote->exists()) {
			throw new NotFoundException(__('Invalid quote'));
		}
		if ($this->Quote->delete()) {
			$this->Session->setFlash(__('Quote deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Quote was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
