﻿<?php
App::uses('AppController', 'Controller');

class MensajesController extends AppController 
{
	var $uses = array('Mensaje','User');
	
	public $paginate = array
	(
        	'limit' => 10, 
        	'order' => 'Mensaje.created DESC'
    );

	function beforeFilter()
	{
		parent::beforeFilter();
	}

	public function index() 
	{
		$this->set('title_for_layout', 'Mensajes');
		$this->Mensaje->recursive = 1;
		$this->set('mensajes', $this->paginate());	
	} 
}
