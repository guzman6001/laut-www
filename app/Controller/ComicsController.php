<?php
App::uses('AppController', 'Controller');

class ComicsController extends AppController 
{
	var $uses = array('Comic','Category','Link');
	
	public $paginate = array(
        	'limit' => 6,
        	'order' => array('Comic.created' => 'desc')
    	);

	function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('index','view','saga');
	}

	public function index() 
	{
		$this->set('title_for_layout', 'Comics');
		if ($this->RequestHandler->isRss() ) 
		{
			$comics = $this->Comic->find('all', 
				array('limit' => 20, 
					'order' => 'Comic.created DESC'));
	        	$this->set(compact('comics'));
		}
		else
		{
			$this->Comic->recursive = 0;
			$this->set('comics', $this->paginate());
			$this->Category->recursive = 1;
			$this->set('categories', $this->paginate('Category',
				array('Category.name' => 'Comicroll')));
		}
	}

	public function saga($sagaslug = null) 
	{
		$this->set('title_for_layout', 'Comics');

		$this->Comic->recursive = 0;
		$this->set('comics', $this->paginate
		(
			'Comic',
			array('Comic.sagaslug' => $sagaslug)
		));
	}
	
	public function view($id = null) 
	{
		$this->set('title_for_layout', 'Comics');
		$this->Comic->id = $id;
		if (!$this->Comic->exists()) {
			throw new NotFoundException(__('Invalid comic'));
		}
		$this->set('comic', $this->Comic->read(null, $id));
	}

	public function add() 
	{
		$this->set('title_for_layout', 'Comics');
		if ($this->request->is('post')) {
			$this->Comic->create();
			if ($this->Comic->save($this->request->data)) {
				$this->Session->setFlash(__('The comic has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The comic could not be saved. Please, try again.'));
			}
		}
	}

	public function edit($id = null) 
	{
		$this->set('title_for_layout', 'Comics');
		$this->Comic->id = $id;
		if (!$this->Comic->exists()) {
			throw new NotFoundException(__('Invalid comic'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Comic->save($this->request->data)) {
				$this->Session->setFlash(__('The comic has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The comic could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Comic->read(null, $id);
		}
	}

	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Comic->id = $id;
		if (!$this->Comic->exists()) {
			throw new NotFoundException(__('Invalid comic'));
		}
		if ($this->Comic->delete()) {
			$this->Session->setFlash(__('Comic deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Comic was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
