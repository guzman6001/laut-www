<?php
App::uses('AppController', 'Controller');

class LationsController extends AppController 
{
	var $name = 'Lation'; 
	var $uses = array('Lation','Category','Tipo','User');
	
	public function nuevo_enlace() 
	{
		$this->set('title_for_layout', 'Nuevo enlace');
		if ($this->request->is('post')) 
		{
			$this->Lation->create();
			$this->Lation->User->id = $this->Auth->user('id');
			$this->Lation->Tipo->id = 1;
			if ($this->Lation->save($this->request->data)) 
			{
				$this->Session->setFlash(__('Enlace guardado satisfactoriamente'));
				$this->redirect(array('controller' => 'enlaces','action' => 'index'));
			} 
			else
			{
				$this->Session->setFlash(__('El enlace no pudo ser guardado '));
				$this->redirect(array('controller' => 'enlaces','action' => 'index'));
			}
		}
		$categories = $this->Lation->Category->find('list');
		$this->set(compact('categories'));
	}
}
